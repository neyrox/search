const util = require('util');
const execute = util.promisify(require('child_process').exec);


async function exec(cmd) {
  console.log("cmd: " + cmd);
  const { stdout, stderr } = await execute(cmd);
  if (stdout)
    console.log('stdout: ', stdout);
  if (stderr)
    console.error('stderr: ', stderr);
  return stdout;
}


async function main() {
  console.log("Checking for updates...");
  await exec("eval $(ssh-agent)");
  await exec("ssh-add ~/.ssh/repo-key");
  const gitRes = await exec("git pull --ff-only");
  if (gitRes.indexOf("changed") > 0) {
    console.log("Updating...");
    await exec("npm update");
    console.log("Restarting...");
    await exec("pm2 restart all --time");
  } else {
    console.log("Nothing to do.");
  }
  console.log("Done.");
}


main();
