const cassandra = require('cassandra-driver');


async function deleteOldIdx(urlHashes, wordHash, dbConn) {
  try {
    const deleteWordRankResPromise = dbConn.execute(
      "DELETE FROM word_rank_by_word_and_url WHERE word_hash=? AND url_hash IN ?",
      [wordHash, urlHashes], {prepare: true});

    await dbConn.execute(
      "DELETE FROM word_posb_by_url_and_word WHERE url_hash IN ? AND word_hash = ?",
      [urlHashes, wordHash], {prepare: true});

    await deleteWordRankResPromise;
  } catch (e) {
    console.error(e);
  }
}



async function runSingle(wordHash, wordStr, dbConn) {
  const before = Date.now();

  const pagesCountRows = (await dbConn.execute("SELECT COUNT(*) FROM word_rank_by_word_and_url WHERE word_hash = ?", [wordHash], {prepare: true})).rows;
  const pagesCount = pagesCountRows[0].count;

  if (pagesCount > 70000) {
    console.log("Word '" + wordStr + "' (" + wordHash + ") is found on " + pagesCount + " pages");

    let result = await dbConn.execute('SELECT url_hash,rank FROM word_rank_by_word_and_url WHERE word_hash = ?', [wordHash], {prepare: true, fetchSize:3000});
    //let rows = result.rows;
    const urlHashAndWordRank = [];

    for await (let row of result)
      urlHashAndWordRank.push({urlHash: row.url_hash.toNumber(), wordRank: row.rank});

    urlHashAndWordRank.sort((a, b) => b.wordRank - a.wordRank);
    const cutRank = urlHashAndWordRank[50000].wordRank;
    console.log("Cut rank is " + cutRank);
    let urlHashesToDelete = [];
    for (let i = 50000; i < urlHashAndWordRank.length; i++) {
      urlHashesToDelete.push(urlHashAndWordRank[i].urlHash);
      if (urlHashesToDelete.length > 999) {
        await deleteOldIdx(urlHashesToDelete, wordHash, dbConn);
        urlHashesToDelete = [];
      }
    }

    if (urlHashesToDelete.length > 0)
      await deleteOldIdx(urlHashesToDelete, wordHash, dbConn);

    console.log("Index cleanup taken " + (Date.now() - before) + "ms");
  }
}


function shuffleArrays(array1, array2) {
  for (let i = array1.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array1[i], array1[j]] = [array1[j], array1[i]];
      [array2[i], array2[j]] = [array2[j], array2[i]];
  }
}


async function main() {
  console.log("Index cleaner started");
  
  process.on('unhandledRejection', (reason/*, promise*/) => {
    console.log('unhandledRejection: ', reason.stack);
    process.exit(1);
  });

  const dbConn = new cassandra.Client({
    contactPoints: ['45.155.207.87', '45.155.207.189'],
    localDataCenter: 'ztv',
    keyspace: 'search',
    credentials: { username: 'crawler', password: 'Le4i6A8i' },
    socketOptions: {readTimeout: 120000}
  });

  const hashes = [];
  const strings = [];
  let result = await dbConn.execute('SELECT * FROM word_by_hash', [], {prepare: true, fetchSize:1000})
  for await (const row of result) {
    hashes.push(row.hash.toNumber());
    strings.push(row.str);
    if (hashes.length % 100000 === 0)
      console.log("Got " + hashes.length + " words...");
    //await runSingle(row.hash.toNumber(), row.str, dbConn);
  }
  console.log("Got all " + hashes.length + " words");

  shuffleArrays(hashes, strings);
  console.log("Words shuffled");

  for (let i = 0; i < hashes.length; ++i) {
    const hash = hashes[i];
    const str = strings[i];
    await runSingle(hash, str, dbConn);
    if (i !== 0 && i % 10000 === 0)
      console.log("Processed " + i + " words...");
  }
  console.log("All done");
}


main();
