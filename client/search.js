var reqUrl = new URL(window.location.href);
reqUrl.protocol = 'http:'
//reqUrl.hostname = '192.168.1.85';
reqUrl.pathname = '/api';

var query = document.getElementById('query');
query.value = reqUrl.searchParams.get('q');

var before = Date.now();

httpGetAsync(reqUrl.toString(), (response) => {
  var after = Date.now();

  var results = document.getElementById('results');
  var debugDiv = document.createElement('div');
  var debugText = document.createElement('p');
  debugText.innerText = 'Query taken ' + (after - before) + 'ms';;
  debugDiv.appendChild(debugText);
  results.appendChild(debugDiv);

  var resultDiv = document.createElement('div');
  for (var item of JSON.parse(response)) {
    var resultDiv = document.createElement('div');
    var title = document.createElement('h2');
    title.setAttribute('class', 'result-title');
    title.innerText = item.title;
    resultDiv.appendChild(title);
    var link = document.createElement('a');
    link.setAttribute('class', 'result-url');
    link.setAttribute('href', item.url);
    link.innerText = trimUrl(item.url);
    resultDiv.appendChild(link);
    var text = document.createElement('p');
    text.setAttribute('class', 'result-text');
    text.innerText = item.text;
    resultDiv.appendChild(text);
    var br = document.createElement('br');
    resultDiv.appendChild(br);
    results.appendChild(resultDiv);
  }
});


function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
}


function trimUrl(url) {
  let lastIdx = url.length - 1;
  if (url[lastIdx] !== '/')
    lastIdx++;

  const delimiterIdx = url.indexOf('://');
  if (delimiterIdx < 0)
    return url.slice(0, lastIdx);  // Wrong url
  else
    return url.slice(delimiterIdx + 3, lastIdx);
}
