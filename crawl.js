"use strict;"

const http = require('http');
const fnv = require('fnv-plus');
const cassandra = require('cassandra-driver');
const Long = cassandra.types.Long;
const random = require('random-bigint');
const promiseTimeout = require('./promiseTimeout.js').promiseTimeout;
const visitor = require('./visitFast.js');
const recentAccess = require('./recentAccess.js');
const textUtil = require('./text.js');
const zlib = require('zlib');
const { promisify } = require('util');
const compress = promisify(zlib.brotliCompress);
const LRU = require("lru-cache");

const prometheusClient = require('prom-client');
// Add a default label which is added to all metrics
prometheusClient.register.setDefaultLabels({
  app: 'crawler'
});
// Enable the collection of default metrics
prometheusClient.collectDefaultMetrics();

const Sentry = require("@sentry/node");
// or use es6 import statements
// import * as Sentry from '@sentry/node';

const Tracing = require("@sentry/tracing");
// or use es6 import statements
// import * as Tracing from '@sentry/tracing';

Sentry.init({
  dsn: "https://0452ae5c32d74a24b89dd831af727344@o471094.ingest.sentry.io/5518191",

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});

const wordsCache = new LRU({ max: 10000 });

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function getRandomInt52() {
  return Math.floor(Math.random() * Math.pow(2,52));
}

function getRandomBigInt() {
  return random(64) - 9223372036854775808n;
  //return Math.floor(Math.random() * Math.pow(2,64));
}

function getRandomFromTo(max) {
  const a = getRandomInt(max);
  const b = getRandomInt(max);
  if (a > b)
    return [b, a];
  else
    return [a, b];
}

function getRandomBool() {
  return Math.random() < 0.5;
}

function getRandomLong() {
  const longNumber = (Math.random() - 0.5) * Math.pow(2,64);
  return Long.fromNumber(longNumber);
}

function getRandomBigIntFromTo() {
  let origin = random(64) - 9223372036854775808n;

  let from = origin - 72057594037927936n;
  let to = origin + 72057594037927936n;

  return [from, to];
}


function getRandomLongFromTo() {
  const [from, to] = getRandomBigIntFromTo();
  return [Long.fromString(from.toString()), Long.fromString(to.toString())];
}


function positionsToBlob(posArr) {
  var uint16PosArr = new Uint16Array(posArr.length);
  uint16PosArr[0] = posArr[0];
  for (let i = 1; i < posArr.length; ++i) {
    uint16PosArr[i] = posArr[i] - posArr[i - 1];
  }
  var posb = Buffer.from(uint16PosArr.buffer);
  return posb;
}


async function savePageContent(urlHash, result, dbConn) {
  const before = Date.now();
  try {
    const bTextPromise = compress(result.text);

    await dbConn.execute(
      "INSERT INTO url_by_hash (hash, ref) values (?, ?)",
      [urlHash, result.url], {prepare: true});

    await dbConn.execute(
      "INSERT INTO page_rank (url_hash,rank,out_links) values (?, ?, ?)",
      [urlHash, 1, result.urls.length], {prepare: true});

    await dbConn.execute(
      "INSERT INTO page_title (url_hash,str) values (?, ?)",
      [urlHash, result.title], {prepare: true});

    const bText = await bTextPromise;
    console.log("Size before: " + Buffer.byteLength(result.text, 'utf8') + ' compressed size: ' + bText.length);
    await dbConn.execute(
      "INSERT INTO page_textb (url_hash,str) values (?, ?)",
      [urlHash, bText], {prepare: true});

    console.log("Saving page content taken " + (Date.now() - before) + "ms");
  } catch (e) {
    console.error(e);
    console.error("Saving page content failed after " + (Date.now() - before) + "ms");
  }
}


async function getRandomPages(blockSet, dbConn) {
  try {
    const dbUrls = []
    const [from, to] = getRandomLongFromTo();
    //console.log("Try from " + from + " to " + to);
    const refRows = (await dbConn.execute("SELECT hash,ref FROM url_by_hash WHERE token(hash) > ? AND token(hash) < ? LIMIT 64", [from, to], {prepare: true})).rows;
    refRows.sort((a, b) => textUtil.countDivisors(a.ref) - textUtil.countDivisors(b.ref));
    for (let row of refRows) {
      const parsedUrl = new URL(row.ref);
      if (blockSet.has(parsedUrl.host)) {
        console.log(row.ref + " is blocked");
        continue;
      }
      if (!textUtil.isDocument(row.ref)) {
        console.log(row.ref + " is not a HTML document");
        continue;
      }
      if (await recentAccess.hasRecentPageAccess(row.ref))
        continue;
      console.log("Got " + row.hash + " | " + row.ref);
      dbUrls.push(row.ref);  
      if (dbUrls.length > 7)
        break;
    }

    console.log("Got " + dbUrls.length + " appropriate urls from Database");

    return dbUrls;
  } catch (e) {
    console.error(e);
    return [];
  }
}


async function getBlockSet(dbConn) {
  try {
    const blockSet = new Set();
    const dbRes = await dbConn.execute("SELECT hostname FROM url_block", [], {prepare: true});
    for await (let row of dbRes)
      blockSet.add(row.hostname);

    console.log("Got " + blockSet.length + " blocked hosts from Database");

    return blockSet;
  } catch (e) {
    console.error(e);
    return new Set();
  }
}


async function deleteLinks(fromHash, hashesToDelete, dbConn) {
  try {
    const deleteFromToResPromise = dbConn.execute(
      "DELETE FROM link_from_to WHERE from_url=? AND to_url IN ?",
      [fromHash, hashesToDelete], {prepare: true});

    await dbConn.execute(
      "DELETE FROM link_to_from WHERE to_url IN ? AND from_url = ?",
      [hashesToDelete, fromHash], {prepare: true});

    await deleteFromToResPromise;
  } catch (e) {
    console.error(e);
  }
}


async function saveLinks(fromHash, toHashes, dbConn) {
  const before = Date.now();
  try {
    const oldRows = (await dbConn.execute("SELECT from_url, to_url FROM link_from_to WHERE from_url=?", [fromHash], {prepare: true})).rows;

    const oldLinks = new Map();
    for (let row of oldRows)
      oldLinks.set(row.to_url.toNumber(), false);

    let addedCount = 0;
    let actualCount = 0;
    let deletedCount = 0;
    let queries = [];
    for (let toHash of toHashes) {
      if (toHash === fromHash)
        continue;
      if (oldLinks.has(toHash))
        oldLinks.set(toHash, true);
      else
        addedCount++;

      queries.push({query: "INSERT INTO link_from_to (from_url, to_url) VALUES (?, ?)", params:[fromHash, toHash]});
      const insertPromise = dbConn.execute("INSERT INTO link_to_from (to_url, from_url) VALUES (?, ?)", [toHash, fromHash], {prepare: true});

      if (queries.length > 255) {
        await insertPromise;
        await dbConn.batch(queries, {prepare: true, logged: false});
        queries = [];
      }
    }

    if (queries.length > 0) {
      await dbConn.batch(queries, {prepare: true, logged: false});
      queries = [];
    }

    let hashesToDelete = [];
    for (let [toHash, isActual] of oldLinks) {
      if (isActual) {
        actualCount++;
        continue;
      }
      hashesToDelete.push(toHash);
      if (hashesToDelete.length > 255) {
        deletedCount += hashesToDelete.length;
        await deleteLinks(fromHash, hashesToDelete, dbConn);
        hashesToDelete = [];
      }
    }

    if (hashesToDelete.length > 0) {
      deletedCount += hashesToDelete.length;
      await deleteLinks(fromHash, hashesToDelete, dbConn);
    }

    console.log("Links added: " + addedCount + " actual: " + actualCount + " deleted: " + deletedCount);
    console.log("Saving page links taken " + (Date.now() - before) + "ms");
  } catch (e) {
    console.error(e);
    console.error("Saving page links failed after " + (Date.now() - before) + "ms");
  }
}


async function saveWords(words, dbConn) {
  const before = Date.now();
  let insertPromise = Promise.resolve();
  try {
    for (let word of words) {
      try {
        const wordHash = fnv.fast1a52(word);

        if (wordsCache.has(wordHash))
          continue;

        wordsCache.set(wordHash, true);
        // Insert new or Update existing, don't wait to finish
        insertPromise = dbConn.execute("INSERT INTO word_by_hash (hash,str) values (?, ?)", [wordHash, word], {prepare: true});
        if (Math.random() < 0.005)  // About 0.5%
          await insertPromise;
      } catch (e) {
        console.error(e);
      }
    }

    // Wait for the last one for sure
    await insertPromise;

    console.log("Saving words taken " + (Date.now() - before) + "ms");
  } catch (e) {
    console.error(e);
    console.error("Saving words failed after " + (Date.now() - before) + "ms");
  }
}


async function deleteOldIdx(urlHash, wordHashesToDelete, dbConn) {
  try {
    const deleteWordRankResPromise = dbConn.execute(
      "DELETE FROM word_rank_by_word_and_url WHERE word_hash IN ? AND url_hash = ?",
      [wordHashesToDelete, urlHash], {prepare: true});

    await dbConn.execute(
      "DELETE FROM word_posb_by_url_and_word WHERE url_hash=? AND word_hash IN ?",
      [urlHash, wordHashesToDelete], {prepare: true});

    await deleteWordRankResPromise;
  } catch (e) {
    console.error(e);
  }
}


async function deleteLowRank(urlHashes, wordHash, dbConn) {
  try {
    const deleteWordRankResPromise = dbConn.execute(
      "DELETE FROM word_rank_by_word_and_url WHERE word_hash=? AND url_hash IN ?",
      [wordHash, urlHashes], {prepare: true});

    await dbConn.execute(
      "DELETE FROM word_posb_by_url_and_word WHERE url_hash IN ? AND word_hash = ?",
      [urlHashes, wordHash], {prepare: true});

    await deleteWordRankResPromise;
  } catch (e) {
    console.error(e);
  }
}


async function cleanupIdx(words, dbConn) {
  const before = Date.now();

  const wordIdx = getRandomInt(words.size);
  let i = 0;
  let wordStr;
  for (let word of words.keys()) {
    if (i === wordIdx) {
      wordStr = word;
      break;
    }
    i++;
  }

  const wordHash = fnv.fast1a52(wordStr);

  const pagesCountRows = (await dbConn.execute("SELECT COUNT(*) FROM word_rank_by_word_and_url WHERE word_hash = ?", [wordHash], {prepare: true})).rows;
  const pagesCount = pagesCountRows[0].count;

  console.log("Word '" + wordStr + "' (" + wordHash + ") is found on " + pagesCount + " pages");

  if (pagesCount > 35000) {
    let result = await dbConn.execute('SELECT url_hash,rank FROM word_rank_by_word_and_url WHERE word_hash = ?', [wordHash], {prepare: true, fetchSize:3000});
    //let rows = result.rows;
    const urlHashAndWordRank = [];

    for await (let row of result)
      urlHashAndWordRank.push({urlHash: row.url_hash.toNumber(), wordRank: row.rank});

    urlHashAndWordRank.sort((a, b) => b.wordRank - a.wordRank);
    const cutRank = urlHashAndWordRank[30000].wordRank;
    console.log("Cut rank is " + cutRank);
    let urlHashesToDelete = [];
    for (let i = 30000; i < urlHashAndWordRank.length; i++) {
      urlHashesToDelete.push(urlHashAndWordRank[i].urlHash);
      if (urlHashesToDelete.length > 999) {
        await deleteLowRank(urlHashesToDelete, wordHash, dbConn);
        urlHashesToDelete = [];
      }
    }

    if (urlHashesToDelete.length > 0)
      await deleteLowRank(urlHashesToDelete, wordHash, dbConn);

    console.log("Index cleanup taken " + (Date.now() - before) + "ms");
  }
}


async function saveIdx(urlHash, words, dbConn) {
  const before = Date.now();
  let insertPromise = Promise.resolve();
  try {
    const oldWordPosRes = await dbConn.execute("SELECT url_hash,word_hash FROM word_posb_by_url_and_word WHERE url_hash=?", [urlHash], {prepare: true, fetchSize:2000});

    const indexedWords = new Map();
    for await (const row of oldWordPosRes)
      indexedWords.set(row.word_hash.toNumber(), false);

    let addedCount = 0;
    let actualCount = 0;
    let deletedCount = 0;
    let queries = [];
    let batchSizeEstimation = 0;
    for (let [word, info] of words) {
      try {
        if (batchSizeEstimation > 10000) {
          await dbConn.batch(queries, {prepare: true, logged: false})
          queries = [];
          batchSizeEstimation = 0;
        }

        const wordHash = fnv.fast1a52(word);
        // console.log("Word " + word + " hash " + wordHash);
        // We already have this page for this word in index,
        // just update it's rank and wordPos
        if (indexedWords.has(wordHash))
          indexedWords.set(wordHash, true);
        else
          addedCount++;

        // Insert new or Update existing
        insertPromise = dbConn.execute("INSERT INTO word_rank_by_word_and_url (word_hash,url_hash,rank) values (?, ?, ?)", [wordHash, urlHash, info.r], {prepare: true});
        if (Math.random() < 0.005)
          await insertPromise;
        const posb = positionsToBlob(info.is);
        queries.push({query: "INSERT INTO word_posb_by_url_and_word (url_hash,word_hash,positions) values (?, ?, ?)", params: [urlHash, wordHash, posb]});
        batchSizeEstimation += 24 + posb.length * 2;
      } catch (e) {
        console.error(e);
      }
    }

    if (queries.length > 0) {
      await dbConn.batch(queries, {prepare: true, logged: false})
      queries = [];
    }

    let wordIdsToDelete = [];
    for (let [wordId, isActual] of indexedWords) {
      if (isActual) {
        actualCount++;
        continue;
      }
      //console.log("Deleting non-actual row for word " + wordId + " and url" + urlHash);
      wordIdsToDelete.push(wordId);
      if (wordIdsToDelete.length > 255) {
        deletedCount += wordIdsToDelete.length;
        await deleteOldIdx(urlHash, wordIdsToDelete, dbConn);
        wordIdsToDelete = [];
      }
    }

    if (wordIdsToDelete.length > 0) {
      deletedCount += wordIdsToDelete.length;
      await deleteOldIdx(urlHash, wordIdsToDelete, dbConn);
    }

    await insertPromise;
    console.log("Word indexes added: " + addedCount + " actual: " + actualCount + " deleted: " + deletedCount);
    console.log("Saving page word indices taken " + (Date.now() - before) + "ms");
  } catch (e) {
    console.error(e);
    console.error("Saving page word indices failed after " + (Date.now() - before) + "ms");
  }
}

async function saveUrls(urls, dbConn) {
  if (urls.length == 0) {
    console.log("Saving urls taken 0 ms");
    return [];
  }
  const before = Date.now();
  let insertPromise = Promise.resolve();
  try {
    const hashes = new Set();
    for (let url of urls) {
      const urlHash = fnv.fast1a52(url);
      if (hashes.has(urlHash))
        continue;

      hashes.add(urlHash);

      // Don't save long links
      if (textUtil.countDivisors(url) > 10)
        continue;

      insertPromise = dbConn.execute("INSERT INTO url_by_hash (hash, ref) values (?, ?)", [urlHash, url], {prepare: true});
      if (Math.random() < 0.005)
        await insertPromise;
    }

    await insertPromise;
    console.log("Saving " + urls.length + " urls taken " + (Date.now() - before) + "ms");
    return [...hashes];
  } catch (e) {
    console.error(e);
    console.error("Saving urls failed after " + (Date.now() - before) + "ms");
    return [];
  }
}


async function save(result, dbConn) {
  console.log("Saving to database...");
  const urlHash = fnv.fast1a52(result.url);
  const savePagePromise = savePageContent(urlHash, result, dbConn);
  const saveIdxPromise = saveIdx(urlHash, result.words, dbConn);
  const saveWordsPromise = saveWords(result.words.keys(), dbConn);
  const cleanupIdxPromise = cleanupIdx(result.words, dbConn);
  const outUrlHashes = await saveUrls(result.urls, dbConn);
  await saveLinks(urlHash, outUrlHashes, dbConn);
  await savePagePromise;
  await saveIdxPromise;
  await saveWordsPromise;
  await cleanupIdxPromise;
}


async function download(url, blockSet, dbConn) {
  console.log('page: ' + url + "...");

  await recentAccess.savePageAccess(url);

  const beforeScraping = Date.now();
  const result = await promiseTimeout(1200000, visitor.scrape(blockSet, url));
  const timestamp = Date.now();
  console.log("Scraping taken " + (timestamp - beforeScraping) + "ms. Words: " + result.words.size + " bad words: " + result.badWords + " block words: " + result.blockWords + " outLinks: " + result.urls.length);
//  console.log(result.words);
  
  if (result.blockWords / result.wordIdx > 0.01) {
    console.warn("Page contains too much blocked words");
    result.urls = [];
  }

  if (result.text.length > 0 && result.words.size > result.badWords && (result.blockWords / result.wordIdx < 0.02)) {
    await save(result, dbConn);
  }

  console.log("Done.");

  return result;
}


function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}


async function run(startUrls, blockSet, dbConn) {
  let urls = startUrls;

  const visited = new Set()
  const maxDepth = 10;

  let i = 0;
  while (urls.length > 0) {

    let rndUrl = urls[Math.floor((Math.random()*urls.length))];

    if (visited.has(rndUrl))
      break;
    visited.add(rndUrl);

    const result = await download(rndUrl, blockSet, dbConn);

    for (let err of result.errors)
      console.error(err);

    shuffleArray(result.urls);
    urls = [];
    for (let outLink of result.urls) {
      if (!await recentAccess.hasRecentPageAccess(outLink))
        urls.push(outLink);
      if (urls.length > 0)
        break;
    }

    console.log('Got ' + urls.length + ' appropriate outLinks');

    i++;
    if (i >= maxDepth)
      break;
  }
}


async function requestListener(req, res) {
  // Retrieve route from request object
  const route = req.url;//new URL(req.url).pathname;
  if (route === '/metrics') {
    // Return all metrics the Prometheus exposition format
    res.setHeader('Content-Type', prometheusClient.register.contentType);
    res.end(await prometheusClient.register.metrics());
  }
}


function startMetricsExporter(server, port) {
  server.listen(port)
    .once('error', function (e) {
      if (e.code != 'EADDRINUSE')
        console.error(e);
      else
        startMetricsExporter(server, port + 1);
    });
}


async function main() {
  const server = http.createServer(requestListener);
  server.once('listening', function() {
    console.log('Metrics exporter started at port ' + server.address().port);
  });
  startMetricsExporter(server, 7901);

  process.on('unhandledRejection', (reason/*, promise*/) => {
    console.log('unhandledRejection: ', reason.stack);
    process.exit(1);
  });

  const dbConn = new cassandra.Client({
    contactPoints: shuffleArray(['45.155.207.87', '45.155.207.189']),
    localDataCenter: 'ztv',
    keyspace: 'search',
    credentials: { username: 'crawler', password: 'Le4i6A8i' }
  });

  console.log("Web crawler started");

  for (;;) {
    try {
      const blockSet = await getBlockSet(dbConn);
      // get unvisited urls from db
      const dbUrls = await getRandomPages(blockSet, dbConn);
      if (dbUrls.length == 0)
        continue;
      await run(dbUrls, blockSet, dbConn);
    } catch (e) {
      console.error(e);
    }
  }
}


main();
