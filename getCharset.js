
function getCharsetFromMeta(node) {
  for (let attr of node.attrs) {
    if (attr.name === 'charset') {
      return attr.value.toUpperCase();
    } else if (attr.name === 'content') {
      // Example: content="text/html; charset=windows-1251"
      const lowerValue = attr.value.toUpperCase();
      const charsetPos = lowerValue.indexOf('CHARSET');
      if (charsetPos > 0) {
        return lowerValue.substring(charsetPos + 8);
      }
    }
  }
}


function getCharsetFromHead(node) {
  for (let child of node.childNodes) {
    if (!child.tagName)
      continue;
    if (child.tagName === 'meta') {
      const res = getCharsetFromMeta(child);
      if (res)
        return res;
    }
  }
}


function getCharsetFromHTML(node) {
  for (let child of node.childNodes) {
    if (!child.tagName)
      continue;
    if (child.tagName === 'head')
      return getCharsetFromHead(child);
  }
}


function getCharset(node) {
  for (let child of node.childNodes) {
    if (!child.tagName)
      continue;
    if (child.tagName === 'html')
      return getCharsetFromHTML(child);    
  }
}

exports.getCharset = getCharset;
