"use strict;"

const http = require('http');
const process = require('process');
process.send = process.send || function () {};

async function requestListener(req, res) {
  console.log(req);
  res.end();
}

function main() {
  const server = http.createServer(requestListener);
  server.listen(2080);
}

main();
