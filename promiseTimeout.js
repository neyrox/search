exports.promiseTimeout = async function(ms, promise) {
  let id;
  let timeout = new Promise((resolve, reject) => {
    id = setTimeout(() => {
      reject('Timed out in ' + ms + 'ms')
    }, ms)
  })

  try {
    // pass the result back
    return await Promise.race([promise, timeout]);
  } finally {
    clearTimeout(id);
  }
}

/*
promiseTimeout(500, new Promise(resolve => {
  resolve()
}))*/