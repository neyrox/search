"use strict;"

const cassandra = require('cassandra-driver');
const Long = cassandra.types.Long;
const random = require('random-bigint');
const process = require('process');
process.send = process.send || function () {};


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}


function getRandomInt52() {
  return Math.floor(Math.random() * Math.pow(2,52));
}


function getRandomFromTo(max) {
  const a = getRandomInt(max);
  const b = getRandomInt(max);
  if (a > b)
    return [b, a];
  else
    return [a, b];
}


function getRandomBigIntFromTo() {
  let origin = random(64) - 9223372036854775808n;

  let from = origin - 72057594037927936n;
  let to = origin + 72057594037927936n;

  return [from, to];
}


function getRandomLongFromTo() {
  const [from, to] = getRandomBigIntFromTo();
  return [Long.fromString(from.toString()), Long.fromString(to.toString())];
}


async function getRandomVisitedPages(dbConn) {
  try {
    const [from, to] = getRandomLongFromTo();

    const rows = (await dbConn.execute("SELECT url_hash FROM page_rank WHERE token(url_hash) > ? AND token(url_hash) < ? LIMIT 256", [from, to])).rows;

    const hashes = [];

    for (let row of rows)
      hashes.push(row.url_hash.toNumber());

    return hashes;
  } catch (e) {
    console.error(e);
    return [];
  }
}


async function countLinksFromPage(urlHash, dbConn) {
  try {
    const rows = (await dbConn.execute("SELECT COUNT(*) FROM link_from_to WHERE from_url = ?", [urlHash], {prepare: true})).rows;

    const result = rows[0].count;

    //console.log('Page ' + urlHash + ' have ' + result + ' outgoing links');

    return result;
  } catch (e) {
    console.error(e);
  }
}


async function getLinksToPage(urlHash, dbConn) {
  try {
    const selectResult = await dbConn.execute("SELECT from_url FROM link_to_from WHERE to_url = ?", [urlHash], {prepare: true});

    const result = [];

    for await (let row of selectResult)
      result.push(row.from_url.toNumber());

    //console.log('Page ' + hash + ' have ' + result.length + ' incoming links');

    return result;
  } catch (e) {
    console.error(e);
  }
}

async function getRankPartsSum(links, dbConn) {
  try {
    let sum = 0.0;

    const res = await dbConn.execute("SELECT rank, out_links FROM page_rank WHERE url_hash IN ?", [links], {prepare: true});

    for await (let row of res) {
      if (row.out_links === 0) {
        sum += row.rank;
        console.error('Got incoming page with zero outLinks');
        continue;
      }
      sum += Math.max(row.rank / row.out_links, 0.001);
    }

    return Math.min(Math.max(sum, 0.01), 1000);
  } catch (e) {
    console.error(e);
    return 0.01;
  }
}


async function updatePageRank(urlHash, rank, dbConn) {
  console.log("Updating page " + urlHash + " rank to " + rank);
  try {
    await dbConn.execute("UPDATE page_rank SET rank = ? WHERE url_hash = ?", [rank, urlHash], {prepare: true});
  } catch (e) {
    console.error(e);
  }
}


async function runMulti(dbConn) {
  const urlHashes = await getRandomVisitedPages(dbConn);
  if (urlHashes.length === 0)
    return 0;

  console.log();
  console.log("Running batch for " + urlHashes.length + " pages");
  for (let urlHash of urlHashes) {
    await run(urlHash, dbConn);
  }

  return urlHashes.length;
}


async function run(urlHash, dbConn) {
  const links = await getLinksToPage(urlHash, dbConn);
  if (links.length === 0)
    return;
  //const outLinksCount = await countLinksFromPage(urlHash, dbConn);
  const newRank = await getRankPartsSum(links, dbConn);
  await updatePageRank(urlHash, newRank, dbConn);
}


async function main() {
  const dbConn = new cassandra.Client({
    contactPoints: ['45.155.207.87', '45.155.207.189'],
    localDataCenter: 'ztv',
    keyspace: 'search',
    credentials: { username: 'crawler', password: 'Le4i6A8i' }
  });

  var running = true;
  /*process.on('SIGINT', function() {
    running = false;
  });*/
  console.log("Page ranker started");
  process.send('ready');

  while (running) {
    const before = Date.now();
    let ranksUpdated = 0;
    try {
      ranksUpdated = await runMulti(dbConn);
    } catch (e) {
      console.error(e);
    } finally {
      const after = Date.now();
      const delta = after - before;
      console.log("Updated " + ranksUpdated + " ranks in " + delta + "ms");
      const sleepDuration = delta * 3;
      console.log("Waiting " + sleepDuration + "ms for next iteration");
      await sleep(sleepDuration);
    }
    //running = false;
  }
}

main();
