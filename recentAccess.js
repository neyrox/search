const Keyv = require('keyv');
const CacheableLookup = require('cacheable-lookup');
const promiseTimeout = require('./promiseTimeout.js').promiseTimeout;

const dnsCacheable = new CacheableLookup();
const cooldown = new Keyv('redis://:X2XAq5EYaYqS1PIUxp15zr26UWsQbu3B@45.155.207.147:6379', { namespace: 'access-cooldown', ttl: 60000 });
const OneDayMs = 86400000;
const OneMinuteMs = 60000;


async function saveHostAccess(hostname) {
  try {
    const addressesPromise = dnsCacheable.lookupAsync(hostname, {all: true}).catch(() => {});
    await cooldown.set(hostname, 'a', OneMinuteMs);
    const records = await promiseTimeout(3000, addressesPromise);
    for (let rec of records)
      await cooldown.set(rec.address, 'a', OneMinuteMs);
  } catch (e) {
    console.error(e);
  }
}

exports.saveHostAccess = saveHostAccess;


exports.savePageAccess = async function(url) {
  try {
    const hostname = new URL(url).hostname;
    const hostAccessPromise = saveHostAccess(hostname);
    await cooldown.set(url, 'a', OneDayMs);
    await hostAccessPromise;
  } catch (e) {
    console.error(e);
  }
}


async function hasRecentHostAccess(hostname) {
  try {
    const addressesPromise = dnsCacheable.lookupAsync(hostname, {all: true}).catch(() => {});
    if (await cooldown.get(hostname) != undefined)
      return true;

    const records = await promiseTimeout(3000, addressesPromise);
    for (let rec of records) {
      if (await cooldown.get(rec.address) != undefined)
        return true;
    }
  } catch (e) {
    console.error("Failed to check access for host " + hostname);
    console.error(e);
    return true;
  }
}

exports.hasRecentHostAccess = hasRecentHostAccess;


exports.hasRecentPageAccess = async function (url) {
  try {
    if (await cooldown.get(url) != undefined)
      return true;

    const hostname = new URL(url).hostname;
    return await hasRecentHostAccess(hostname);
  } catch (e) {
    console.error("Failed to check access for url " + url);
    console.error(e);
    return true;
  }
}
