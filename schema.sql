-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for search
CREATE DATABASE IF NOT EXISTS `search` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;
USE `search`;

-- Dumping structure for table search.host
CREATE TABLE IF NOT EXISTS `host` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `lastAccess` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lastAccessIndex` (`lastAccess`) USING BTREE,
  KEY `nameIndex` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table search.idx
CREATE TABLE IF NOT EXISTS `idx` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wordId` int(11) unsigned NOT NULL DEFAULT 0,
  `linkId` int(10) unsigned NOT NULL DEFAULT 0,
  `rank` float NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique` (`wordId`,`linkId`),
  KEY `LinkIdIndex` (`linkId`) USING HASH,
  KEY `RankIndex` (`rank`) USING BTREE,
  KEY `WordIndex` (`wordId`) USING BTREE,
  CONSTRAINT `wordKey` FOREIGN KEY (`wordId`) REFERENCES `word` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Data exporting was unselected.

-- Dumping structure for table search.link
CREATE TABLE IF NOT EXISTS `link` (
  `from` int(10) unsigned NOT NULL DEFAULT 0,
  `to` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`from`,`to`),
  KEY `FromIndex` (`from`) USING HASH,
  KEY `ToIndex` (`to`) USING HASH,
  CONSTRAINT `FK_links_url` FOREIGN KEY (`from`) REFERENCES `url` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_links_url_2` FOREIGN KEY (`to`) REFERENCES `url` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Data exporting was unselected.

-- Dumping structure for table search.page
CREATE TABLE IF NOT EXISTS `page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `urlId` int(10) unsigned NOT NULL DEFAULT 0,
  `rank` float NOT NULL,
  `lastVisit` bigint(20) NOT NULL,
  `outLinks` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `LastVisitIndex` (`lastVisit`) USING BTREE,
  KEY `UrlIndex` (`urlId`) USING BTREE,
  CONSTRAINT `FK_page_url` FOREIGN KEY (`urlId`) REFERENCES `url` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Data exporting was unselected.

-- Dumping structure for table search.text
CREATE TABLE IF NOT EXISTS `text` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageId` int(10) unsigned NOT NULL,
  `str` longtext CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_text_page` (`pageId`),
  CONSTRAINT `FK_text_page` FOREIGN KEY (`pageId`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Data exporting was unselected.

-- Dumping structure for table search.url
CREATE TABLE IF NOT EXISTS `url` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(1024) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `RefIndex` (`ref`(768))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Data exporting was unselected.

-- Dumping structure for table search.word
CREATE TABLE IF NOT EXISTS `word` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `str` varchar(512) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `StrIndex` (`str`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table search.wordpos
CREATE TABLE IF NOT EXISTS `wordpos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idxId` int(10) unsigned NOT NULL,
  `positions` longtext CHARACTER SET utf8mb4 NOT NULL DEFAULT '0' CHECK (json_valid(`positions`)),
  PRIMARY KEY (`id`),
  KEY `FK_wordpos_idx` (`idxId`),
  CONSTRAINT `FK_wordpos_idx` FOREIGN KEY (`idxId`) REFERENCES `idx` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
