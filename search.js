"use strict;"

const fnv = require('fnv-plus');
const http = require('http');
const cassandra = require('cassandra-driver');
const process = require('process');
process.send = process.send || function () {};
const zlib = require('zlib');
const { promisify } = require('util');
const decompress = promisify(zlib.brotliDecompress);
const textUtil = require('./text.js');
const LRU = require("lru-cache");
const MurmurHash3 = require('imurmurhash');

const wordCacheTTL = 1000 * 60 * 10;  // 10 minutes
const wordRankCache = new LRU({ max: 100, maxAge: wordCacheTTL });
const wordPosCache = new LRU({ max: 20000, maxAge: wordCacheTTL });

const pageRankCacheTTL = 1000 * 60 * 60;  // 1 hour
const pageRankCache = new LRU({ max: 10000, maxAge: pageRankCacheTTL });

const Sentry = require("@sentry/node");
// or use es6 import statements
// import * as Sentry from '@sentry/node';

const Tracing = require("@sentry/tracing");
// or use es6 import statements
// import * as Tracing from '@sentry/tracing';

Sentry.init({
  dsn: "https://0452ae5c32d74a24b89dd831af727344@o471094.ingest.sentry.io/5518191",

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});


const dbConn = new cassandra.Client({
  contactPoints: ['45.155.207.87', '45.155.207.189'],
  localDataCenter: 'ztv',
  keyspace: 'search',
  credentials: { username: 'searcher', password: 'FVh3UOwn' }
});


var negativeFilter = new Set();

async function updateNegativeFilter() {
  const before = Date.now();
  const promise1 = addWordUrls('порн', dbConn, negativeFilter);
  const promise2 = addWordUrls('porn', dbConn, negativeFilter);
  const promise3 = addWordUrls('porno', dbConn, negativeFilter);

  await promise1;
  await promise2;
  await promise3;

  console.log("Negative filter update taken " + (Date.now() - before) + "ms. " + negativeFilter.size + " urls are prohibited");
}


setInterval(pageRankCacheCleanup, 60013);
setInterval(wordRankCacheCleanup, 62233);
setInterval(wordPosCacheCleanup, 64997);


function pageRankCacheCleanup() {
  pageRankCache.prune();
}


function wordRankCacheCleanup() {
  wordRankCache.prune();
}


function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}


function wordPosCacheCleanup() {
  const timeBefore = Date.now();
  const countBefore = wordPosCache.itemCount;
  wordPosCache.prune();
  const timeAfter = Date.now();
  const countAfter = wordPosCache.itemCount;
  if (countAfter != countBefore)
    console.log("Cleaning wordPos cache taken "+ (timeAfter - timeBefore) + "ms. Count before: " + countBefore + " count after: " + countAfter);
}


async function addWordUrls(word, dbConn, urlSet) {
  const before = Date.now();
  const wordHash = fnv.fast1a52(word);
  let row;
  try {
    const selectWordsQuery = "SELECT url_hash from word_rank_by_word_and_url WHERE word_hash = ?";

    let res = await dbConn.execute(selectWordsQuery, [wordHash], {prepare: true, fetchSize:4000});

    for await (row of res)
      urlSet.add(row.url_hash.toNumber());

    console.log("getWordUrls for word " + word + " (" + wordHash + ") taken " + (Date.now() - before) + "ms");
  } catch (e) {
    console.log(row);
    console.error(e);
  }
}


async function getWordRanks(word, dbConn) {
  if (wordRankCache.has(word)) {
    console.log("Links for word " + word + " found in cache");
    return wordRankCache.get(word);    
  }

  const before = Date.now();
  const wordHash = fnv.fast1a52(word);
  let row;
  try {
    const selectWordsQuery = "SELECT * from word_rank_by_word_and_url WHERE word_hash = ?";

    const res = await dbConn.execute(selectWordsQuery, [wordHash], {prepare: true, fetchSize:2000});

    const links = [];
    for await (row of res)
      links.push({l:row.url_hash.toNumber(), r:row.rank, hash:wordHash});

    wordRankCache.set(word, links);
    console.log("getWordRanks for word " + word + " (" + wordHash + ") taken " + (Date.now() - before) + "ms and " + links.length + " results were found ");
    return links;
  } catch (e) {
    console.log(row);
    console.error(e);
    return [];
  }
}


function positionsFromBlob(posb) {
  var uint16PosDeltaArr = new Uint16Array(posb.buffer, posb.byteOffset, posb.length / Uint16Array.BYTES_PER_ELEMENT);
  if (uint16PosDeltaArr.length === 0)
    return []
  const posArr = [uint16PosDeltaArr[0]];
  for (let i = 1; i < uint16PosDeltaArr.length; ++i) {
    posArr.push(posArr[i - 1] + uint16PosDeltaArr[i]);
  }
  return posArr;
}


function composeStrKey(key1, key2) {
  return key1.toString() + '-' + key2.toString();
}


async function getWordPositions(urlHashes, wordHashes, dbConn) {
  const before = Date.now();
  let row;
  try {
    const positionMap = new Map();
    const urlHashesToRequest = new Set();
    const wordHashesToRequest = new Set();
    for (let urlHash of urlHashes) {
      for (let wordHash of wordHashes) {
        const cacheKey = composeStrKey(urlHash, wordHash);
        if (wordPosCache.has(cacheKey)) {
          if (positionMap.has(urlHash)) {
            positionMap.get(urlHash).set(wordHash, wordPosCache.get(cacheKey));
          } else {
            const wordPositions = new Map();
            wordPositions.set(wordHash, wordPosCache.get(cacheKey));
            positionMap.set(urlHash, wordPositions);
          }
        } else {
          urlHashesToRequest.add(urlHash);
          wordHashesToRequest.add(wordHash);
        }
      }      
    }

    if (urlHashesToRequest.size > 0) {
      const selectPosQuery = "SELECT * from word_posb_by_url_and_word WHERE url_hash IN ? AND word_hash IN ?";
      const res = await dbConn.execute(selectPosQuery, [[...urlHashesToRequest], [...wordHashesToRequest]], {prepare: true, fetchSize:550});

      for await (row of res) {
        const urlHash = row.url_hash.toNumber();
        const wordHash = row.word_hash.toNumber();
        const positions = positionsFromBlob(row.positions);
        if (positionMap.has(urlHash)) {
          positionMap.get(urlHash).set(wordHash, positions);
        } else {
          const wordPositions = new Map();
          wordPositions.set(wordHash, positions);
          positionMap.set(urlHash, wordPositions);
        }
        const cacheKey = composeStrKey(urlHash, wordHash);
        wordPosCache.set(cacheKey, positions);
      }  
    }

    console.log("getWordPositions tanken " + (Date.now() - before) + "ms");
    return positionMap;
  } catch (e) {
    console.log(row);
    console.error(e);
    return new Map();
  }
}


async function getPageRanks(urls, dbConn) {
  const before = Date.now();
  try {
    const ranks = [];
    const urlHashesToRequest = [];
    for (let urlHash of urls) {
      if (pageRankCache.has(urlHash))
        ranks.push({url_hash: urlHash, rank:pageRankCache.get(urlHash)});
      else
        urlHashesToRequest.push(urlHash);
    }

    if (urlHashesToRequest.length === 0)
      return ranks;

    const result = await dbConn.execute("SELECT url_hash,rank FROM page_rank WHERE url_hash IN ?", [urlHashesToRequest], {prepare: true, fetchSize:1000});
    for await (const row of result) {
      const urlHash = row.url_hash.toNumber();
      ranks.push({url_hash: urlHash, rank: row.rank});
      pageRankCache.set(urlHash, row.rank);
    }

    console.log("getPageRanks taken " + (Date.now() - before) + "ms");
    return ranks;
  } catch (e) {
    console.error(e);
    return [];
  }
}


async function getPageText(urlHash, dbConn) {
  try {
    return (await dbConn.execute("SELECT * FROM page_textb WHERE url_hash = ?", [urlHash], {prepare: true})).rows;
  } catch (e) {
    console.error(e);
    return [];
  }
}


async function getUrls(links, dbConn) {
  try {
    const rows = (await dbConn.execute("SELECT * FROM url_by_hash WHERE hash IN ?", [links], {prepare: true})).rows;
    const urlByHash = new Map();
    for (let row of rows) {
      urlByHash.set(row.hash.toNumber(), row.ref);
    }
    return urlByHash;
  } catch (e) {
    console.error(e);
    return new Map();
  }
}


async function getTitles(links, dbConn) {
  try {
    const rows =  (await dbConn.execute("SELECT * FROM page_title WHERE url_hash IN ?", [links], {prepare: true})).rows;
    const titleByHash = new Map();
    for (let row of rows) {
      titleByHash.set(row.url_hash.toNumber(), row.str);
    }
    return titleByHash;
  } catch (e) {
    console.error(e);
    return new Map();
  }
}


async function getFullPageTexts(urls, dbConn) {
  const before = Date.now();

  const textsByUrl = new Map();
  const dbQueries = [];
  for (let urlHash of urls) {
    dbQueries.push(getPageText(urlHash, dbConn));
    textsByUrl.set(urlHash, '');
  }

  for (let dbQuery of dbQueries) {
    const rows = await dbQuery;
    if (rows.length > 0) {
      const row = rows[0];
      const text = (await decompress(row.str)).toString("utf8");
      textsByUrl.set(row.url_hash.toNumber(), text);
    }
  }

  console.log("Get Full Page Texts taken " + (Date.now() - before) + "ms");

  return textsByUrl;
}



function getShortPageDescs(words, links, fullTextsByUrl) {
  const special = ' +-=.,;:|';
  const textsByUrl = new Map();
  for (let urlHash of links) {
    textsByUrl.set(urlHash, '');
  }

  for (let [urlHash, text] of fullTextsByUrl) {
    let min = text.length;
    let max = 0;
    const lcText = text.toLowerCase();
    for (let word of words) {
      let n = lcText.indexOf(word);
      // If not found
      if (n < 0)
        continue;
      // If it is not beginning of the word
      if (n > 0 && !special.includes(lcText[n-1]))
        continue;
      if (n < min)
        min = n;
      if (n + word.length > max)
        max = n + word.length;
    }
    if (max - min < 200) {
      min = Math.max(0, min - 100);
      max = Math.min(lcText.length - 1, max + 100);
    } else if (max - min > 1000) {
      max = min + 1000;
    }
    let shortText;
    if (max > min)
      shortText = text.slice(min, max);
    else
      shortText = text.slice(0, 100);
    textsByUrl.set(urlHash, shortText);
  }

  return textsByUrl;
}


function printEmpty(res) {
  res.write(JSON.stringify([]), 'utf-8');
}

function findDuplicates(urlHashes, texts) {
  const _1 = Date.now();
  const arrShingles = new Map();
  const setShingles = new Map();

  for (let [urlHash, text] of texts) {
    const tokens = textUtil.tokenize(text);
  
    if (tokens.length < 8)
      continue;

    arrShingles.set(urlHash, []);

    for (let i = 0; i < tokens.length - 8; ++i) {
      const hashState = new MurmurHash3();
      hashState.hash(tokens[i]);
      for (let j = 1; j < 8; j++) {
        hashState.hash(' ');
        hashState.hash(tokens[i + j]);
      }
      arrShingles.get(urlHash).push(hashState.result());
    }

    const shArr = arrShingles.get(urlHash);
    const shSet = new Set();
    for (let sh of shArr)
      shSet.add(sh);
    setShingles.set(urlHash, shSet);
  }

  const _2 = Date.now();
  console.log("Shilngles calculation taken " + (_2 - _1) + "ms");

  const duplicates = new Set();

  for (let i = 0; i < urlHashes.length - 1; ++i) {
    const urlHash = urlHashes[i];
    const shArr = arrShingles.get(urlHash);

    const overlaps = new Map();
    for (let j = i + 1; j < urlHashes.length; ++j)
      overlaps.set(urlHashes[j], 0);

    for (let j = 0; j < 50; ++j) {
      const rndSh = shArr[getRandomInt(shArr.length)];
      for (let k = i + 1; k < urlHashes.length; ++k) {
        const otherUrlHash = urlHashes[k];
        if (setShingles.get(otherUrlHash).has(rndSh))
          overlaps.set(otherUrlHash, overlaps.get(otherUrlHash) + 1);
      }
    }

    for (let [urlHash, count] of overlaps) {
      if (count > 40)
        duplicates.add(urlHash);
    }
  }

  const _3 = Date.now();
  console.log("Duplicate detection taken " + (_3 - _2) + "ms");

  return duplicates;
}



async function print(words, urlHashes, dbConn, res) {
  const fullTextsPromise = getFullPageTexts(urlHashes, dbConn);
  const titlesPromise = getTitles(urlHashes, dbConn);
  const urlsPromise = getUrls(urlHashes, dbConn);
  const fullTexts = await fullTextsPromise;
  const texts = getShortPageDescs(words, urlHashes, fullTexts);
  const duplicates = findDuplicates(urlHashes, fullTexts);
  const titles = await titlesPromise;
  const urls = await urlsPromise;

  const pages = [];
  for (let urlHash of urlHashes) {
    if (duplicates.has(urlHash))
      continue;

    const page = {url:'', title:'', text:''};
    if (urls.has(urlHash))
      page.url = urls.get(urlHash);
    if (texts.has(urlHash))
      page.text = texts.get(urlHash);
    if (titles.has(urlHash))
      page.title = titles.get(urlHash);

    pages.push(page);

    // Cut to max 20 non-duplicates
    if (pages.length >= 20)
      break;
  }

  res.write(JSON.stringify(pages), 'utf-8');
}


async function applyWordPositions(allWordIndexes, best1000PagesSet, wordsRanksMap, dbConn) {
  const urlHashes = new Set();
  const wordHashes = new Set();
  for (let wordIdx of allWordIndexes) {
    if (!best1000PagesSet.has(wordIdx.l))
      continue;

    urlHashes.add(wordIdx.l);
    wordHashes.add(wordIdx.hash);
  }

  const urlHashToWordHashToPositions = await getWordPositions(urlHashes, wordHashes, dbConn);
  const urlHashToWordPositions = new Map();
  for (let wordIdx of allWordIndexes) {
    if (!best1000PagesSet.has(wordIdx.l))
      continue;
    if (!urlHashToWordHashToPositions.has(wordIdx.l))
      continue;
    const wordHashToPositions = urlHashToWordHashToPositions.get(wordIdx.l);
    if (!wordHashToPositions.has(wordIdx.wordHash))
      continue;
    const positions = wordHashToPositions.get(wordIdx.wordHash);
    if (urlHashToWordPositions.has(wordIdx.l))
      urlHashToWordPositions.set(wordIdx.l, urlHashToWordPositions.get(wordIdx.l).concat(positions));
    else
      urlHashToWordPositions.set(wordIdx.l, positions);
  }

  for (let [l, indices] of urlHashToWordPositions) {
    indices.sort((a, b) => a - b);
    for (let i = 1; i < indices.length; ++i) {
      let delta = indices[i] - indices[i-1];
      // Same word, different parts
      if (delta == 0)
        continue;
      // Too far away, no bonus
      // TODO: tune this parameter
      if (delta > 5)
        continue;
      let rankBonus = 0.5 / (delta * indices[i]);
      wordsRanksMap.set(l, wordsRanksMap.get(l) + rankBonus);
    }
  }
}


async function run(query, res) {
  console.log(query);

  const lst = textUtil.tokenize(query.toLowerCase());

  const searchQuery = [];
  for (let word of lst) {
    let term = word;
    if (word.length > 1 && !textUtil.isSpecial(word) && !textUtil.isHex(word))
      term = textUtil.stem(word);
    if (term && !searchQuery.includes(term))
      searchQuery.push(term);
  }
  console.log(searchQuery);

  // Don't wait for it
  dbConn.execute('INSERT INTO query (tid, str, parsed) VALUES (now(), ?, ?)', [query, JSON.stringify(searchQuery)], {prepare: true});

  if (searchQuery.length === 0) {
    printEmpty(res);
    return;
  }

  const negativeFilterPromise = updateNegativeFilter();

  const dbRequests = [];
  for (let word of searchQuery) {
    const dbRequest = getWordRanks(word, dbConn);
    dbRequests.push(dbRequest);
    //dbRequests.push(Promise.resolve([]));
  }

  await negativeFilterPromise;

  const urlWordCount = new Map();
  const allWordIndexes = [];
  for (let dbRequest of dbRequests) {
    const wordIndexes = await dbRequest;
    const wordUrls = new Set();
    for (let wordIdx of wordIndexes) {
      if (negativeFilter.has(wordIdx.l))
        continue;
      wordUrls.add(wordIdx.l);
      allWordIndexes.push(wordIdx);
    }
    for (let wordUrl of wordUrls) {
      if (urlWordCount.has(wordUrl))
        urlWordCount.set(wordUrl, urlWordCount.get(wordUrl) + 1);
      else
        urlWordCount.set(wordUrl, 1);
    }
  }

  // Nothing found
  if (urlWordCount.size === 0) {
    printEmpty(res);
    return;
  }

  const pagesPerWordCount = new Map();
  for (let [url, cnt] of urlWordCount) {
    if (pagesPerWordCount.has(cnt))
      pagesPerWordCount.get(cnt).add(url);
    else
      pagesPerWordCount.set(cnt, new Set([url]));
  }
  
  const maxWordCountPerPage = Math.max(...pagesPerWordCount.keys());
  console.log("Maximum count of words per page is " + maxWordCountPerPage);

  const bestPages = pagesPerWordCount.get(maxWordCountPerPage);
  console.log("Count of best pages is " + bestPages.size);

  const wordsRanksMap = new Map();
  for (let wordIdx of allWordIndexes) {
    if (!bestPages.has(wordIdx.l))
      continue;
    if (wordsRanksMap.has(wordIdx.l))
      wordsRanksMap.set(wordIdx.l, wordsRanksMap.get(wordIdx.l) + wordIdx.r);
    else
      wordsRanksMap.set(wordIdx.l, wordIdx.r);
  }
  const best1000Pages = [...wordsRanksMap.keys()].sort((a, b) => wordsRanksMap.get(b) - wordsRanksMap.get(a)).slice(0, 999);
  const best1000PagesSet = new Set(best1000Pages);
  const webPageRankPromise = getPageRanks(best1000Pages, dbConn);

  if (maxWordCountPerPage > 1) {
    await applyWordPositions(allWordIndexes, best1000PagesSet, wordsRanksMap, dbConn);
  }

  // ranks by links between pages
  const webPageRanks = await webPageRankPromise;
  for (let row of webPageRanks) {
    const urlHash = row.url_hash;
    wordsRanksMap.set(urlHash, wordsRanksMap.get(urlHash) + row.rank);
  }

  // Sort pages by rank
  let pageRanksArr = [...wordsRanksMap].sort((a, b) => b[1] - a[1]);

  // Get maximum first 40
  pageRanksArr = pageRanksArr.slice(0, 40);

  const justLinks = [];
  for (let page of pageRanksArr) {
    justLinks.push(page[0]);
  }

  await print(searchQuery, justLinks, dbConn, res);
}


async function requestListener(req, res) {
  const url = new URL(req.url, 'http://' + req.headers.host);
  //console.log(url);
  res.writeHead(200, {'Content-Type': 'application/json'});

  if (url.pathname != '/api' || !url.searchParams.has('q')) {
    console.error('Bad request');
    res.end();
    return;
  }

  const q = url.searchParams.get('q');
  console.log('Query: ' + q);

  try {
    await run(q, res);
  } catch (e) {
    console.error(e);
  }
  res.end();
}


function main() {
  const server = http.createServer(requestListener);

  server.listen(1080);

  console.log("Search service started");
}


main();
