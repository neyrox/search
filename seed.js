"use strict;"

var fnv = require('fnv-plus');
const cassandra = require('cassandra-driver');
//const vgmUrl = 'https://www.vgmusic.com/music/console/nintendo/nes';
//'https://devgamm.com/',

const seedUrls = [
'https://www.jetbrains.com/',
'https://www.unrealengine.com/en-US/',
'https://developer.mozilla.org/en-US/',
'https://www.edx.org/',
'http://htmlbook.ru/',
'https://graphics.stanford.edu/',
'https://xakep.ru/',
'https://tproger.ru/',
'https://rsdn.org/',
'https://www.npmjs.com/',
'https://gamedev.ru/',
'https://www.cnews.ru/',
'https://www.linux.org.ru/',
'https://leetcode.com/',
'https://www.geeksforgeeks.org/',
'https://www.udemy.com/',
'https://www.udacity.com/',
'https://praktikum.yandex.ru/',
'https://www.reddit.com/r/programming/',
'https://www.w3schools.com/',
'https://habr.com/',
'https://news.ycombinator.com/',
'https://www.coursera.org/',
'https://www.digitalocean.com/',
'https://www.digitalocean.com/community',
'https://www.digitalocean.com/community/tutorials',
'https://www.linode.com/',
'https://aws.amazon.com/',
'https://docs.microsoft.com/en-us/',
'https://support.apple.com/',
'https://developers.google.com/',
'https://en.wikipedia.org/wiki/Computer_programming',
'https://en.wikipedia.org/wiki/Computer_science',
'https://en.wikipedia.org/wiki/Software_development',
'https://en.wikipedia.org/wiki/Robot',
'https://stackexchange.com/sites#technology',
'https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/',
'https://opennet.ru/',
'https://www.sql.ru/',
'http://vanilla-js.com/',
'http://ontico.ru/',
'https://mariadb.com/',
'https://www.postgresql.org/',
'https://www.mongodb.com/',
'https://redis.io/',
'https://www.nix.ru/',
'https://www.python.org/',
'https://developer.amd.com/',
'https://software.intel.com/content/www/us/en/develop/home.html',
'https://www.comnews.ru/',
'https://appleinsider.ru/',
'https://www.ferra.ru/',
'https://3dnews.ru/',
'https://roem.ru/',
'https://hi-tech.mail.ru/',
'http://tdaily.ru/',
'https://www.darkreading.com/',
'https://krebsonsecurity.com/',
'https://www.schneier.com/',
'https://arstechnica.com/',
'https://www.technologyreview.com/',
'https://www.nytimes.com/wirecutter/',
'https://www.khanacademy.org/',
'https://css-tricks.com/',
'https://www.smashingmagazine.com/',
'https://hackernoon.com/',
'https://www.oreilly.com/',
'https://rusetfs.com/landing',
'https://fintraining.livejournal.com/',
'https://lurkmore.to/',
'https://blog.codinghorror.com/',
'https://en.wikipedia.org/wiki/Microsoft_Windows',
'https://en.wikipedia.org/wiki/Linux',
'https://ubuntu.com/',
'https://www.centos.org/',
'https://getfedora.org/',
'https://ruhighload.com/',
'https://geekbrains.ru/',
'http://gramota.ru/',
'https://www.sphinx-doc.org/',
'http://sphinxsearch.com/',
'https://www.tarantool.io/en/',
'https://www.tarantool.io/ru/',
'https://qna.habr.com/',
'https://www.gnu.org/',
'https://opensource.com/',
'https://cassandra.apache.org/',
'http://citforum.ru/',
'https://sharpc.livejournal.com/',
'https://www.fileformat.info/index.htm',
'https://etcd.io/',
'https://docs.datastax.com/',
'https://memcached.org/',
'https://hackware.ru/',
'https://www.debian.org/',
'https://itsecforu.ru/',
'https://docs.unity3d.com/Manual/index.html',
'https://linuxize.com/',
'https://pm2.keymetrics.io/',
'https://linuxhint.com/',
'https://www.poftut.com/',
'https://www.archlinux.org/',
'https://www.linuxatemyram.com/',
'https://www.dzhang.com/index.php',
'https://thelastpickle.com/',
'https://apache.org/',
'https://www.cloudwalker.io/',
'https://www.percona.com/',
'https://www.instaclustr.com/',
'https://wordpress.com/',
'https://1c.ru/',
'https://www.1c-bitrix.ru/',
'https://www.joomla.org/',
'https://www.opencart.com/',
'https://www.drupal.org/',
'https://www.wix.com/',
'https://modx.com/',
'https://dle-news.ru/',
'https://typo3.org/',
'https://www.msu.ru/',
'https://mipt.ru/',
'https://www.hse.ru/',
'https://en.wikipedia.org/wiki/List_of_hash_functions',
'http://www.cse.yorku.ca/~oz/hash.html',
'https://www.bell-labs.com/',
'https://overcoder.net/',
'https://en.wikipedia.org/wiki/Inverted_index',
'https://en.wikipedia.org/wiki/Programming_language',
'https://en.wikipedia.org/wiki/C_(programming_language)',
'https://en.wikipedia.org/wiki/C_Sharp_(programming_language)',
'https://en.wikipedia.org/wiki/JavaScript',
'https://en.wikipedia.org/wiki/Node.js',
'https://prometheus.io/',
'https://grafana.com/',
'https://en.wikipedia.org/wiki/Prime_number',
'https://www.elastic.co/',
'https://pixonic.com/ru',
'https://pixonic.com/en',
'https://eu.wargaming.net/en',
'https://ru.wargaming.net/ru',
'https://www.atlassian.com/',
'https://www.devtodev.com/',
'https://www.opensourcecms.com/',
'https://en.wikipedia.org/wiki/Pixonic',
'https://ru.wikipedia.org/wiki/Pixonic',
'https://hh.ru/',
'https://habr.com/ru/company/pixonic/',
'https://www.educative.io/edpresso',
'https://en.wikipedia.org/wiki/Favicon',
'https://aliev.me/',
'http://aliev.me/runestone/index.html',
'https://www.tutorialspoint.com/index.htm',
'https://en.wikipedia.org/wiki/List_of_programming_languages_by_type',
'https://www.liveinternet.ru/',
'https://4pda.ru/',
'https://overclockers.ru/',
'http://www.thg.ru/',
'https://www.cyberforum.ru/',
'https://driver.ru/',
'http://www.oszone.net/',
'https://losst.ru/',
'https://www.liveinternet.ru/rating/ru/',
'https://wiki.ubuntu.com/',
'https://www.boost.org/',
'https://informatics.ru/',
'https://www.datastax.com/',
'https://ru.wikipedia.org/wiki/Животные',
'https://en.wikipedia.org/wiki/Biology',
'https://ru.wikipedia.org/wiki/Растения',
'https://ru.wikipedia.org/wiki/Plant',
'https://ru.wikipedia.org/wiki/Искусство',
'https://ru.wikipedia.org/wiki/Art',
'https://ru.wikipedia.org/wiki/Культура',
'https://ru.wikipedia.org/wiki/Culture',
'https://ru.wikipedia.org/wiki/Наука',
'https://ru.wikipedia.org/wiki/Science',
'https://megabook.ru/',
'https://ilibrary.ru/',
'https://thrift.apache.org/',
'https://developers.google.com/protocol-buffers',
'https://msgpack.org/index.html',
'https://www.banki.ru/',
'https://www.tecmint.com/',
'http://aushestov.ru/things-to-follow/',
'https://learnyousomeerlang.com/',
'http://learnyouahaskell.com/',
'https://thealphacentauri.net/',
'https://nplus1.ru/',
'https://www.mercurial-scm.org/'
]


async function addSeedPage(url, dbConn) {
  try {
    const hash = fnv.fast1a52(url);
    const res = await dbConn.execute("SELECT * FROM url_by_hash WHERE hash = ?", [hash], {prepare: true});
    const rows = res.rows;

    if (rows.length === 0)
      console.log(url + ' added to the database');
    else
      console.log(url + ' is already in the database');

    await dbConn.execute("INSERT INTO url_by_hash (hash, ref) VALUES (?, ?);", [hash, url], {prepare: true});
  } catch (e) {
    console.error(e);
  }
}


async function run(dbConn) {
  for (let url of seedUrls) {
      await addSeedPage(url, dbConn);
  }
}


async function main() {
  const dbConn = new cassandra.Client({
    contactPoints: ['45.155.207.87', '45.155.207.189'],
    localDataCenter: 'ztv',
    keyspace: 'search',
    credentials: { username: 'crawler', password: 'Le4i6A8i' }
  });

  console.log('Seeding page database');
  await run(dbConn);
  console.log('Done');
  process.exit(0);
}


main();
