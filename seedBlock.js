"use strict;"

const cassandra = require('cassandra-driver');

const pornoUrls = [
  'kiss18.ru',
  'podrochi.online',
  'drochila.online',
  'konchil.online',
  'tytporno.online',
  'ukrafoto.net',
  'pornhub.com',
  'zadrochi.net',
  '2porno.online',
  'traher.online',
  'porno999.online',
  'xxx777.tv',
  'porno999.online',
  'zadrot.name',
  'trahaem.net',
  'achievo.org',
  'rumama.tv',
  'drocha.tv',
  'faper.online',
  'xxx333.tv',
  'www.uzx.su',
  'zatrahal.online',
  'zrelochki.ru',
  'findver.ru',
  'all-sec.ru',
  'trucktorist.ru',
  'leng-travel.ru',
  'narkozdravmed.ru',
  'sms4505.ru',
  'vetervideo.ru',
  'zodbux.ru',
  'voltgenerator.ru',
  'porno-zrelyh-volosatye.bmtk230106.ru',
  'tuvlin.ru',
  'timkashop.ru',
  'centrgurr.ru',
  'sakha1c.ru',
  'marina-shevchenko.ru',
  'russianpop.ru',
  'domashne-rus-porno.top',
  'domashnee-porno-anal.ru',
  'analdin.ru',
  'analnoe-porno.net',
  'huyamba.org',
  'ebasex.net',
  'vshoot.ru',
  'zrelki.cc',
  'chastnoe-ruskoe-porno.top',
  'chastnoe-porn-video.ru',
  'timezyx.ru',
  'cookplits.ru',
  'anal-po-russki.ru',
  '1porno.online'
]

const casinoUrls = [
  'cazino-777-bonus.com',
  'top-casino-slot.azurewebsites.net',
  'casino.ru',
  '1wawx.top',
  'vulcankasino.rocks',
  'guru-casino.appspot.com',
  '1298joycasino.com',
  'casino-x1250.com',
  'championslots.io',
  'mw7playdom.win',
  'socialgame2.live',
  'ace.deluxecasino.club',
  'jet3.casino',
  'vegas-grand4.com',
  'www.fastpay-casino18.com',
  'fresh123.casino',
  'sol68.casino',
  'rox-jsukuqjxx.com',
  'casinoclubcolumb.club',
  'drift-cazino.today',
  'frankvip.me',
  'mrbit.solutions',
  'casino-slotv.com',
  'sslcasino.xyz',
  'casino-aplay.life',
  'casino-playfortuna50ld.com',
  'booi6xl25l.com'
]

const noncontentUrls = [
  'adclick.g.doubleclick.net',
  'yabs.yandex.ru',
  'ads.google.com',
  'redirect.appmetrica.yandex.com',
  'oauth.vk.com',
  'connect.ok.ru'
]


async function addBlackPage(url, comment, dbConn) {
  try {
    await dbConn.execute("INSERT INTO url_block (hostname, comment) VALUES (?, ?);", [url, comment], {prepare: true});
  } catch (e) {
    console.error(e);
  }
}


async function run(dbConn) {
  for (let url of pornoUrls) {
      await addBlackPage(url, 'porno', dbConn);
  }
  for (let url of casinoUrls) {
    await addBlackPage(url, 'casino', dbConn);
  }
  for (let url of noncontentUrls) {
    await addBlackPage(url, 'non-content', dbConn);
  }
}


async function main() {
  const dbConn = new cassandra.Client({
    contactPoints: ['45.155.207.87', '45.155.207.189'],
    localDataCenter: 'ztv',
    keyspace: 'search',
    credentials: { username: 'crawler', password: 'Le4i6A8i' }
  });

  console.log('Seeding block database');
  await run(dbConn);
  console.log('Done');
  process.exit(0);
}


main();
