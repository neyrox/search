const getCharset = require('../getCharset.js').getCharset;
const parse5 = require('parse5');
const assert = require('assert');

function prepare(html) {
  const document = parse5.parse(html);
}


const metaCharset =
'<!DOCTYPE html>\
<html>\
<head>\
<meta charset="UTF-8"/>\
<title>Наука — Википедия</title>\
</head>\
<body></body>\
</html>';

const metaContent1 =
'<HTML lang="ru">\
<HEAD>\
<TITLE>Н. В. Гоголь. Мертвые души</TITLE>\
<meta name="viewport" content="width=device-width, initial-scale=1.0">\
<META NAME="Keywords" CONTENT="русская классическая литература интернет-библиотека Алексея Комарова текст Николай Гоголь Мертвые души">\
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">\
</HEAD>\
<BODY></BODY>\
</HTML>';


const metaContent2 =
'<HTML>\
<HEAD>\
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=koi8-r">\
   <TITLE>OpenNet без "www" - минималистский вариант портала по открытому ПО, Linux, BSD и Unix системам</TITLE>\
   <meta name="KeyWords" content="статьи программы документация unix linux freebsd solaris bsd информация cisco настройка новости утилиты сеть perl программирование описание администрирование русификация безопасность">\
   <meta name="viewport" content="width=device-width, initial-scale=1">\
   <meta content="/opennet_icon_128.png" itemprop="image">\
</HEAD>\
<BODY></BODY>\
</HTML>';


describe('getCharset', function() {
  it('should parse charset from meta-charset', function() {
    assert.strictEqual('UTF-8', getCharset(parse5.parse(metaCharset)));
  });
  it('should parse charset from meta-content', function() {
    assert.strictEqual('WINDOWS-1251', getCharset(parse5.parse(metaContent1)));
    assert.strictEqual('KOI8-R', getCharset(parse5.parse(metaContent2)));
  });
});
