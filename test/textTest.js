const textUtil = require('../text.js');
const assert = require('assert');


describe('isHex', function() {
  it('should return false if alien characters present', function() {
    assert(!textUtil.isHex('1x2'));
    assert(!textUtil.isHex('e-1'));
    assert(!textUtil.isHex('990абв'));
  });
  it('should return true for decimal number', function() {
    assert(textUtil.isHex('123456789'));
    assert(textUtil.isHex('4072420'));
  });
  it('should return true for hexadecimal number', function() {
    assert(textUtil.isHex('abcdef0123456789'));
    assert(textUtil.isHex('9676d5f8'));
    assert(textUtil.isHex('990da22da967'));
  });
  it('should ignore hash at the beginning', function() {
    assert(textUtil.isHex('#9085163'));
    assert(textUtil.isHex('#ac2d'));
    assert(textUtil.isHex('#10df'));
  });
  it('should ignore 0x at the beginning', function() {
    assert(textUtil.isHex('0x9085163'));
    assert(textUtil.isHex('0xac2d'));
    assert(textUtil.isHex('0x10df'));
  });
  it('should return false for just # or 0x', function() {
    assert(!textUtil.isHex('#'));
    assert(!textUtil.isHex('0x'));
  });
});


describe('trimHex', function() {
  it('should return same if there is nothing to clean', function() {
    assert.strictEqual('abcdef0123456789', textUtil.trimHex('abcdef0123456789'));
    assert.strictEqual('9676d5f8', textUtil.trimHex('9676d5f8'));
    assert.strictEqual('990da22da967', textUtil.trimHex('990da22da967'));
    assert.strictEqual('123456789', textUtil.trimHex('123456789'));
    assert.strictEqual('4072420', textUtil.trimHex('4072420'));
  });
  it('should cut hash in the beginning', function() {
    assert.strictEqual('abcdef0123456789', textUtil.trimHex('#abcdef0123456789'));
    assert.strictEqual('9676d5f8', textUtil.trimHex('#9676d5f8'));
    assert.strictEqual('990da22da967', textUtil.trimHex('#990da22da967'));
    assert.strictEqual('123456789', textUtil.trimHex('#123456789'));
    assert.strictEqual('9085163', textUtil.trimHex('#9085163'));
  });
  it('should cut 0x in the beginning', function() {
    assert.strictEqual('abcdef0123456789', textUtil.trimHex('0xabcdef0123456789'));
    assert.strictEqual('9676d5f8', textUtil.trimHex('0x9676d5f8'));
    assert.strictEqual('990da22da967', textUtil.trimHex('0x990da22da967'));
    assert.strictEqual('123456789', textUtil.trimHex('0x123456789'));
    assert.strictEqual('9085163', textUtil.trimHex('0x9085163'));
  });
});


describe('tokenize', function() {
  it('should split by space', function() {
    assert.deepStrictEqual(['c++', '123', 'c#'], textUtil.tokenize('C++ 123 C#'));
  });
  it('should split by quotes', function() {
    assert.deepStrictEqual(['a', '1', 'x'], textUtil.tokenize('\'a\'"1"`x`'));
  });
  it('should split by newlines', function() {
    assert.deepStrictEqual(['a', 'b', 'c', 'd', 'e'], textUtil.tokenize('a\rb\nc\td\r\ne\n\n'));
  });
  it('should split by +-=*/\\|', function() {
    assert.deepStrictEqual(['1', '2', '3', '4', '5', '6', '7', '8'], textUtil.tokenize('1+2-3*4/5=6\\7|8'));
  });
  it('should split by ~1@#$%^&*', function() {
    assert.deepStrictEqual(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'], textUtil.tokenize('a~b!c@d#e$f%g^h&i*j'));
  });
  it('should split by <>()[]{}', function() {
    assert.deepStrictEqual(['a', 'b', 'c', 'd', 'e', 'f'], textUtil.tokenize('(a[b]){c(d)}[e]<f>'));
  });
  it('should split by .,:;', function() {
    assert.deepStrictEqual(['a', 'b', 'c', 'd', 'e'], textUtil.tokenize('a.b,c:d;e'));
  });
  it('should save separate characters', function() {
    assert.deepStrictEqual(['#', '+', '|', '$', '%'], textUtil.tokenize('# + | $ %'));
  });
});


describe('detectLanguage', function() {
  it('should detect english words', function() {
    assert.deepStrictEqual('en', textUtil.detectLanguage('people'));
    assert.deepStrictEqual('en', textUtil.detectLanguage('would'));
    assert.deepStrictEqual('en', textUtil.detectLanguage('about'));
    assert.deepStrictEqual('en', textUtil.detectLanguage('lyrics'));
  });
  it('should detect russian words', function() {
    assert.deepStrictEqual('ru', textUtil.detectLanguage('человек'));
    assert.deepStrictEqual('ru', textUtil.detectLanguage('время'));
    assert.deepStrictEqual('ru', textUtil.detectLanguage('жизнь'));
    assert.deepStrictEqual('ru', textUtil.detectLanguage('русский'));
  });
  it('should detect mixed words', function() {
    assert.deepStrictEqual('', textUtil.detectLanguage('гoд'));
    assert.deepStrictEqual('', textUtil.detectLanguage('рукa'));
    assert.deepStrictEqual('', textUtil.detectLanguage('двeрь'));
  });
});


describe('isDocument', function() {
  it('should detect html', function() {
    assert(textUtil.isDocument('link.htm'));
    assert(textUtil.isDocument('link.html'));
    assert(textUtil.isDocument('link'));
    assert(textUtil.isDocument('link./'));
  });
  it('should detect plain text', function() {
    assert(textUtil.isDocument('link.txt'));
  });
  it('should detect images', function() {
    assert(!textUtil.isDocument('image.jpg'));
    assert(!textUtil.isDocument('image.gif'));
    assert(!textUtil.isDocument('image.png'));
    assert(!textUtil.isDocument('image.jpeg'));
    assert(!textUtil.isDocument('image.svg'));
    assert(!textUtil.isDocument('image.eps'));
  });
  it('should detect audio', function() {
    assert(!textUtil.isDocument('file.mid'));
    assert(!textUtil.isDocument('file.vaw'));
    assert(!textUtil.isDocument('file.mp3'));
    assert(!textUtil.isDocument('file.ogg'));
  });
  it('should detect video', function() {
    assert(!textUtil.isDocument('file.avi'));
    assert(!textUtil.isDocument('file.mp4'));
    assert(!textUtil.isDocument('file.mov'));
    assert(!textUtil.isDocument('file.webm'));
  });
  it('should detect non-plain-text-or-html', function() {
    assert(!textUtil.isDocument('file.pdf'));
    assert(!textUtil.isDocument('file.rtf'));
    assert(!textUtil.isDocument('file.doc'));
    assert(!textUtil.isDocument('file.docx'));
  });
  it('should detect presentations', function() {
    assert(!textUtil.isDocument('file.ppt'));
    assert(!textUtil.isDocument('file.pptx'));
  });
  it('should detect archives', function() {
    assert(!textUtil.isDocument('file.zip'));
    assert(!textUtil.isDocument('file.rar'));
    assert(!textUtil.isDocument('file.tar'));
    assert(!textUtil.isDocument('file.gz'));
    assert(!textUtil.isDocument('file.7z'));
    assert(!textUtil.isDocument('file.iso'));
    assert(!textUtil.isDocument('file.img.xz'));
    assert(!textUtil.isDocument('file.rar'));
  });
  it('should detect mobole application package', function() {
    assert(!textUtil.isDocument('file.apk'));
    assert(!textUtil.isDocument('file.ipa'));
  });
});



describe('isWrongLanguage', function() {
  it('should return false on Russian', function() {
    assert(!textUtil.isWrongLanguage('ru-RU'));
    assert(!textUtil.isWrongLanguage('ru'));
    assert(!textUtil.isWrongLanguage('Ru'));
    assert(!textUtil.isWrongLanguage('rU'));
    assert(!textUtil.isWrongLanguage('RU'));
    assert(!textUtil.isWrongLanguage('Russian'));
  });
  it('should return false on Engish', function() {
    assert(!textUtil.isWrongLanguage('en-US'));
    assert(!textUtil.isWrongLanguage('en-UK'));
    assert(!textUtil.isWrongLanguage('en'));
    assert(!textUtil.isWrongLanguage('En'));
    assert(!textUtil.isWrongLanguage('eN'));
    assert(!textUtil.isWrongLanguage('EN'));
    assert(!textUtil.isWrongLanguage('English'));
  });
  it('should return false on Empty string', function() {
    assert(!textUtil.isWrongLanguage(''));
  });
  it('should return true on all unknown', function() {
    assert(textUtil.isWrongLanguage('es'));
    assert(textUtil.isWrongLanguage('it'));
    assert(textUtil.isWrongLanguage('ar'));
    assert(textUtil.isWrongLanguage('ch'));
  });
});
