"use strict;"

const natural = require('natural');
const iconv = require("iconv-lite");

const urlPartRankFactor = 100;
const hostnamePartRankFactor = 100;

/*
exports.areAllUrlCharactersAllowed = function(url) {
  try {
    url = decodeURI(url);
  } catch (e) {
    // Some "broken" URL
    return false;
  }

  const loweredUrl = url.toLowerCase();
  // \u2013\u2014 - long dashes
  const allowed = 'abcdefghijklmnopqrstuvwxyz0123456789абвгдеёжзийклмнопрстуфхцчшщъыьэюя :/.,-+?&=_#()[]{}%\'"<>^@~$`!*\u2013\u2014';

  for (let chr of url) {
    if (allowed.includes(chr.toLowerCase()))
      continue;

    console.log("URL " + url + " contains not allowed character " + chr);

    return false;
  }

  return true;
}
*/


const russianLetters = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';
const englishLetters = 'abcdefghijklmnopqrstuvwxyz';
const numbers = '0123456789';

const hexNumbers = '0123456789abcdef';
const special = [
  'c#', 'c++',
  '++', '--', '!=', '<>', '==', '>=', '<=', '+=', '-=', '*=', '/=', '&=', '|=',
  '^=', '>>', '<<', '::', '->', '=>', '&&', '||', '...', '//',
  '!===', '===', '>>>',
  '[]', '()', '{}'];
const specialSet = new Set(special);


const wordBlockList = new Set([
  'ху', 'хуев', 'хуйн', 'наху', 'доху', 'нахуяч', 'выхуячива', 'хуямб',
  'бляд', 'блядк', 'блядск',
  'порн', 'порнушк', 'порнух',
  'милф',
//  'секс',
  'шлюх',
  'инцест',
  'ана', 'анальн',
  'еб', 'еба', 'ебл', 'ебучк', 'заеба', 'выеба',
  'дроч', 'дрочев',
  'манд',
  'групповух',
  'пизд',
  'дилд',
  'фистинг',
  'камшот',
  'траха',
  'cum',
  'cunt',
  'sex', 'fuck', 'fucker',
  'cunt',
  'porno', 'porn', 'xxx',
  'anal',
  'dildo',
  'whore', 'slut', 'hooker',
  'bitch'
]);


function isWordInBlockList(word) {
  return wordBlockList.has(word);
}

exports.isWordInBlockList = isWordInBlockList;


function isSpecial(str) {
  return specialSet.has(str);
}

exports.isSpecial = isSpecial;


function isHex(str) {
  let start = 0;
  if (str.startsWith('0x')) {
    if (str.length > 2)
      start = 2;
    else
      return false;
  } else if (str.startsWith('#')) {
    if (str.length > 1)
      start = 1;
    else
      return false;
  }

  for (let i = start; i < str.length; i++) {
    if (!hexNumbers.includes(str[i]))
      return false;
  }
  return true;
}

exports.isHex = isHex;

function trimHex(str) {
  if (str.startsWith('0x'))
    return str.substr(2);
  else if (str.startsWith('#'))
    return str.substr(1);
  return str;
}

exports.trimHex = trimHex;

function tokenize(str) {
  const result = [];
  // Split by space-like characters
  const lst1 = str.toLowerCase().split(/[ \r\n\t]+/).filter(w => w);
  for (let token1 of lst1) {
    if (token1.length === 1) {
      result.push(token1);
      continue;
    }
    if (isSpecial(token1)) {
      result.push(token1)
      continue;
    }
    if (isHex(token1)) {
      result.push(trimHex(token1));
      continue;
    }

    // Intermediate step - punctuation (dots, quotes, commas)
    //...
  
    // Last step - non-alphanumeric characters
    // \u00A0 - nbsp
    // \u202F - narrow no break space
    // \u2028 - line separator
    // \u200E - left-to-right mark
    // \u200B - zero-width space
    const lst2 = token1.split(/[×+#`´'′"“”‘’″«»‹›„‚~|\-=*±.:,;!@?$£€₽%&<>≥≤()[\]{}^\\/№…­­—–―­­­−‐‑_§©®™°→↓↑\u00A0\u2028\u200E\u202F\u200B]+/).filter(w => w);
    for (let token2 of lst2) {
      if (token1.length == 1) {
        result.push(token1);
        continue;
      }
      if (isHex(token1)) {
        result.push(trimHex(token1));
        continue;
      }

      result.push(token2)
    }
  }

  return result;
}

exports.tokenize = tokenize;


function detectLanguage(str) {
  if (special.includes(str))
    return '';
  if (str.length == 1)
    return '';

  try {
    let rus = 0;
    let eng = 0;
    //let num = 0;
    for (let char of str) {
      if (englishLetters.includes(char)) {
        eng++;
      } else if (russianLetters.includes(char)) {
        rus++;
      } else if (numbers.includes(char)) {
        //num++;
      } else {
        return 'err';
      }
    }

    if (eng > 0 && rus === 0)
      return 'en';
    else if (rus > 0 && eng == 0)
      return 'ru';

    return '';
  } catch (e) {
    return '';
  }
}

exports.detectLanguage = detectLanguage;


function stem(word) {
  const lang = detectLanguage(word);

  if (lang === 'err') {
    //console.log("Bad word: '" + word + "'");
    return undefined;
  } else if (lang === 'en') {
    return natural.PorterStemmer.stem(word);
  } else if (lang === 'ru') {
    return natural.PorterStemmerRu.stem(word);
  } else if (isHex(word)) {
    return trimHex(word);
  } else if (word.length > 1) {  // Save index of separate characters
    return undefined;
  }
}

exports.stem = stem;


function canAddSpace(src)
{
  if (src.length == 0)
    return false;

  if (src[src.length-1] == ' ')
    return false;

  return true;
}

exports.canAddSpace = canAddSpace;


exports.cleanup = function(text) {
  const cut = ' \r\n\t';
  //const known = 'abcdefghijklmnopqrstuvwxyz0123456789абвгдеёжзийклмнопрстуфхцчшщъыьэюя+-=#.,;:|\'';
  let result = "";

  for(let i = 0; i < text.length; ++i) {
    const chr = text[i];
    if (cut.includes(chr)) {
        if (canAddSpace(result))
          result += ' ';
    }
    else {
      result += chr;
    }
  }

  return result.trim();
}


exports.cleanupURL = function(text) {
  const known = 'abcdefghijklmnopqrstuvwxyz0123456789абвгдеёжзийклмнопрстуфхцчшщъыьэюя';
  let result = "";

  for(let i = 0; i < text.length; ++i) {
    const chr = text[i];
    if (known.includes(chr.toLowerCase()))
      result += chr;
    else if (exports.canAddSpace(result))
      result += ' ';
  }

  return result.trim();
}


function toRank(idx) {
  return Math.max(1.0 - 0.0001 * idx, 0.0001) + Math.random() * 0.0001;  // Add some jitter for better (lower) cardinality
}

exports.toRank = toRank;


exports.addWordsFromUrl = function(url, words) {
  const parsedUrl = new URL(url);
  const hostnameParts = tokenize(parsedUrl.hostname);
  for (let i = 0; i < hostnameParts.length; i++) {
    const word = hostnameParts[i];
    const term = stem(word);
    if (!term)
      continue;

    const rank = hostnamePartRankFactor + Math.random() * 0.0001;
    if (words.has(term)) {
      const prev = words.get(term);
      words.set(term, {r:prev.r + rank, is:prev.is});
    } else {
      words.set(term, {r:rank, is:[]});
    }
  }

  const decodedUrl = decodeURI(url).toLowerCase();

  const urlParts = tokenize(decodedUrl);

  for (let i = 0; i < urlParts.length; i++) {
    let word = urlParts[i];
    if (!isSpecial(word)) {
      word = stem(word);
      if (!word)
        continue;  
    }

    const rank = toRank(i) * urlPartRankFactor / urlParts.length;
    if (words.has(word)) {
      const prev = words.get(word);
      words.set(word, {r:prev.r + rank, is:prev.is});
    } else {
      words.set(word, {r:rank, is:[]});
    }
  }
}


exports.clampURL = function(urlStr) {
  urlStr = urlStr.split('#')[0];

  if (urlStr.length < 256)
    return urlStr;

  urlStr = urlStr.split('&')[0];

  if (urlStr.length < 256)
    return urlStr;

  return urlStr.split('?')[0];
}


const badExt = new Set([
  'gif', 'jpg', 'png', 'jpeg','svg', 'eps',
  'mid', 'vaw', 'mp3', 'ogg',
  'avi', 'mp4', 'mov', 'webm',
  'zip', 'rar', 'tar', 'gz', '7z', 'iso', 'xz',
  'pdf', 'rtf', 'doc', 'docx',
  'ppt', 'pptx',
  'apk', 'ipa', 'msi'
]);


exports.isDocument = function(path) {
  const lastDotPos = path.lastIndexOf('.');
  if (lastDotPos < 0)
    return true;

  const ext = path.substr(lastDotPos + 1).toLowerCase();
  if (badExt.has(ext))
    return false;

  return true;
}


exports.countDivisors = function(url){
  const divisors = "/&?";
  let count = 0;
  for (let char of url) {
    if (divisors.includes(char))
      count++;
  }
  return count;
}


function isWrongLanguage(lang) {
  if (lang.length == 0)
    return false;  // We don't know
  lang = lang.toLowerCase();
  return !(lang.startsWith('en') || lang.startsWith('ru'));
}

exports.isWrongLanguage = isWrongLanguage;


function checkEncoding(name) {
  return (name || '')
      .toString()
      .trim()
      .replace(/^latin[-_]?(\d+)$/i, 'ISO-8859-$1')
      .replace(/^win(?:dows)?[-_]?(\d+)$/i, 'WINDOWS-$1')
      .replace(/^utf[-_]?(\d+)$/i, 'UTF-$1')
      .replace(/^ks_c_5601-1987$/i, 'CP949')
      .replace(/^us[-_]?ascii$/i, 'ASCII')
      .toUpperCase();
}


function toUtf8(str, charset) {
  charset = checkEncoding(charset || 'UTF-8');

  str = str || '';
  str = Buffer.from(str, 'binary');

  return iconv.decode(str, charset);
}

exports.toUtf8 = toUtf8;


