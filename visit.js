"use strict";

const puppeteer = require('puppeteer');
const textUtil = require('./text.js');


// This function executes in browser context, so it have some additional things defined
/* eslint-env browser */
function scrape() {
  let txt = '';
  if (document.title)
    txt += document.title;

  if (document.body && document.body.innerText)
    txt += ' ' + document.body.innerText;

  const result = {text:txt, urls: [], errors: []}

  const items = document.querySelectorAll('a');

  items.forEach((item) => {
      try {
        if (!isEmpty(item.href) && item.getAttribute("href")[0] != '#' && item.getAttribute("href").toLowerCase() !== 'about:blank') {
          let url = new URL(item.href.split('#')[0]);
          if ((url.protocol == 'http:' || url.protocol == 'https:') && isDocument(url.pathname))
            result.urls.push(clamp(url.toString()));
        }
      } catch(e) {
        result.errors.push(e);
      }
    });

  function clamp(urlStr) {
    if (urlStr.length < 256)
      return urlStr;

    urlStr = urlStr.split('&')[0];

    if (urlStr.length < 256)
      return urlStr;

    return urlStr.split('?')[0];
  }

  function isDocument(path) {
    if (path.endsWith('.gif') || path.endsWith('.jpg') || path.endsWith('.png') || path.endsWith('.jpeg'))
      return false;

    if (path.endsWith('.mid') || path.endsWith('.vaw') || path.endsWith('.mp3') || path.endsWith('.ogg'))
      return false;

    if (path.endsWith('.avi') || path.endsWith('.mp4'))
      return false;

    if (path.endsWith('.zip') || path.endsWith('.rar') || path.endsWith('.gz') || path.endsWith('.7z'))
      return false;

    return true;
  }

  function isEmpty(str) {
      return (!str || 0 === str.length || !str.trim());
  }

  return result;
}
/*eslint-env node*/

function toRank(idx) {
  return 10.0 / idx;
}

function countWords(url, text) {
  const lst = text.toLowerCase().split(/[ .:,;]+/).filter(w => w);

  let list = [];
  let idx = 1;
  for(let word of lst) {
    if (word.length < 2)
      continue;
    let dec = 0;
    if (word.length < 128) {
      list.push({w:word, i:idx++});
      dec = 1;
    }
    let subWords = word.split(/[+\-=|]+/).filter(w => w);
    if (subWords.length > 1) {
      idx -= dec;
      for(let subWord of subWords) {
        if (subWord.length < 2)
          continue;
        if (subWord.length < 128)
          list.push({w:subWord, i:idx++});
        else
          list.push({w:subWord.slice(0, 127), i:idx++});
      }
    }
  }

  let words = new Map();

  textUtil.addWordsFromUrl(url, words);

  for(let item of list) {
    if (words.has(item.w)) {
      let val = words.get(item.w);
      val.r += toRank(item.i);
      val.is.push(item.i);
      words.set(item.w, val);
    } else {
      words.set(item.w, {r:toRank(item.i), is:[item.i]});
    }
  }

  return words;
}

function merge(result, frameResult) {
  result.text += ' ' + frameResult.text;
  result.urls.push.apply(result.urls, frameResult.urls);
}

async function dumpFrameTree(frame, result, isMain) {
  if (frame.url().length == 0)
    return;
  if (frame.url().split(':')[0].slice(0,4).toLowerCase() !== 'http')
    return;

  if (!isMain)
    console.log('frame: ' + frame.url() + "...");

  let frameResult = await frame.evaluate(scrape);
  merge(result, frameResult);

  for (const child of frame.childFrames()) {
    await dumpFrameTree(child, result, false);
  }
}

async function scrapeUnsafe(a, url, browser) {
  const page = await browser.newPage();
  await page.goto(url);
  var b = Date.now();
  console.log("Got main page in " + (b-a) + "ms");
 //await page.waitForTimeout(5000);

  let lang = (await page.evaluate(() => {
    /*eslint-env browser*/
    return document.documentElement.lang;
    /*eslint-env node*/
  })).toLowerCase();
  console.log(lang);
  console.log("URL: " + page.url());
  if (lang.length > 0 && !(lang.startsWith('ru') || lang.startsWith('en'))) {
    console.log('Wrong language!');
    return {text:'', urls:[], words: new Map(), errors: []};
  }

  let result = {text:'', urls:[], errors: []};

  await dumpFrameTree(page.mainFrame(), result, true);

  result.text = textUtil.cleanup(result.text);
  result.words = countWords(url, result.text);
  result.text = result.text.slice(0, 1000000);
  result.urls = [...new Set(result.urls)];

  return result;
}

exports.scrape = async function (url) {
  let browser;
  try {
    var a = Date.now();
    browser = await puppeteer.launch();
    return await scrapeUnsafe(a, url, browser);
  } catch (e) {
    console.error(e);
    return {text:'', urls:[], words: new Map(), errors: []};
  } finally {
    await browser.close();
  }
};


(async () => {
  var a = Date.now();
  let res = await exports.scrape('https://news.ycombinator.com/');
  var b = Date.now();
  for (let [w, inf] of res.words)
    inf.is = JSON.stringify(inf.is);
  //console.log(res);
  //console.log("Done in" + (b-a) + "ms");
  //let words = [...res.words.keys()].sort();
  //console.log(words);
  process.exit(0);
})();
