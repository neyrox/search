"use strict;"

const robotsParser = require('robots-parser');
const parse5 = require('parse5');
const textUtil = require('./text.js');
const recentAccess = require('./recentAccess.js');
const natural = require('natural');
const getCharset = require('./getCharset.js').getCharset;
const cld = require('cld');
const pureGot = require('got');
const got = pureGot.extend({
	hooks: {
		afterResponse: [
			(response/*, retryWithMergedOptions*/) => {
        //console.log("Headers: " + JSON.stringify(response.headers));

        const contentType = response.headers['content-type'];

        if (!contentType)
          return response;

        if (contentType.startsWith('text') || contentType.startsWith('application/xhtml+xml'))
          return response;

        throw new Error('Wrong content type ' + contentType);
			}
		]
	},
	mutableDefaults: true
});

const Keyv = require('keyv');

const LRU = require("lru-cache");

const CacheableLookup = require('cacheable-lookup');

const dnsCacheable = new CacheableLookup();

const robotsCacheTTL = 1000 * 60 * 60 * 8;  // Eight hours
const pageCacheTTL = 1000 * 60 * 60 * 1;  // One hour

const robotsTxtCache = new LRU({ max: 1000, maxAge: robotsCacheTTL });

const robotsRedis = new Keyv('redis://:X2XAq5EYaYqS1PIUxp15zr26UWsQbu3B@45.155.207.147:6379', { ttl: robotsCacheTTL });
const redis = new Keyv('redis://:X2XAq5EYaYqS1PIUxp15zr26UWsQbu3B@45.155.207.147:6379', { ttl: pageCacheTTL });

const userAgent = 'MyBot/0.1';
//const userAgent = 'Mozilla/5.0 (compatible; MyBot/0.1;)';
const gotHeaders = {
  'user-agent': userAgent
}
const gotParams = {
  headers: gotHeaders,
  cache: redis,
  dnsCache: dnsCacheable,
  timeout: 30000,
  throwHttpErrors: false,
  encoding: 'binary'
};
const robotsGotParams = {
  headers: gotHeaders,
  cache: robotsRedis,
  dnsCache: dnsCacheable,
  timeout: 10000,
  throwHttpErrors: false
};


const titleRankFactor = 80;

const nodeRankFactors = new Map();
nodeRankFactors.set('h1', 50);
nodeRankFactors.set('h2', 40);
nodeRankFactors.set('h3', 30);
nodeRankFactors.set('h4', 20);
nodeRankFactors.set('h5', 15);
nodeRankFactors.set('h6', 14);
nodeRankFactors.set('h7', 13);
nodeRankFactors.set('h8', 12);
nodeRankFactors.set('h9', 11);


function getNodeRankFactor(nodeName) {
  if (nodeRankFactors.has(nodeName))
    return nodeRankFactors.get(nodeName);
  else
    return 1.0;
}


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


async function getRobotsTxt(robotsUrl) {
  const allowAll = {isAllowed: () => {return true;}};
  try
  {
    const hostname = new URL(robotsUrl).hostname;
    if (await recentAccess.hasRecentHostAccess(hostname))
      await sleep(1000);

    const robotsResponse = await pureGot(robotsUrl, robotsGotParams);
   // console.log(robotsResponse.body);

    if (robotsResponse.statusCode !== 200) {
      console.log("Got " + robotsResponse.statusCode + " trying to get robots.txt by url " + robotsUrl);
      return allowAll;
    }

    if (!robotsResponse.isFromCache)
      await recentAccess.saveHostAccess(hostname);

    return robotsParser(robotsUrl, robotsResponse.body);
  } catch (e) {
    console.log(robotsUrl);
    console.error(e);
    return allowAll;
  }
}


async function getFromRedisCache(url) {
  const req = await redis.get("cacheable-request:GET:" + url);
  if (!req)
    return undefined;
  
  const parsedReq = JSON.parse(req);

  if (parsedReq.value.statusCode !== 200)
    return undefined;

  const bodyBase64 = parsedReq.value.body;
  if (!bodyBase64.startsWith(':base64:'))
    return undefined;

  const buff = Buffer.from(bodyBase64.substring(8), 'base64');
  const text = buff.toString('utf-8');
  return text;
}


async function getRobotsTxtFast(robotsUrl) {
  const allowAll = {isAllowed: () => {return true;}};
  try
  {
    const text = await getFromRedisCache(robotsUrl);
    if (!text)
      return allowAll;

    return robotsParser(robotsUrl, text);
  } catch (e) {
    console.log(robotsUrl);
    console.error(e);
    return allowAll;
  }
}


async function isAllowed(url) {
  if (url == null || url == 'null') {
    console.log('nullUrl');
    return false;
  }

  const parsedUrl = new URL(url);
  const robotsUrl  = parsedUrl.origin + '/robots.txt';

  if (robotsTxtCache.has(robotsUrl))
    return robotsTxtCache.get(robotsUrl);

  const robotsTxtParser = await getRobotsTxt(robotsUrl);
  robotsTxtCache.set(robotsUrl, robotsTxtParser);

  return robotsTxtParser.isAllowed(url, userAgent);
}


async function isAllowedFast(url) {
  if (url == null || url == 'null') {
    console.log('nullUrl');
    return false;
  }

  const parsedUrl = new URL(url);
  const robotsUrl  = parsedUrl.origin + '/robots.txt';

  if (robotsTxtCache.has(robotsUrl))
    return robotsTxtCache.get(robotsUrl);

  let robotsTxtParser;
  // In ~1% cases do it for real to refill cache
  if (Math.random() < 0.01)
    robotsTxtParser = await getRobotsTxt(robotsUrl);
  else
    robotsTxtParser = await getRobotsTxtFast(robotsUrl);
  robotsTxtCache.set(robotsUrl, robotsTxtParser);

  return robotsTxtParser.isAllowed(url, userAgent);
}


function addWord(word, factor, result) {
  if (!textUtil.isSpecial(word)) {
    const lang = textUtil.detectLanguage(word);

    if (lang === 'err') {
      console.log("Bad word: '" + word + "'");
      result.badWords++;
      return;
    } else if (lang === 'en') {
      word = natural.PorterStemmer.stem(word);
    } else if (lang === 'ru') {
      word = natural.PorterStemmerRu.stem(word);
    } else if (textUtil.isHex(word)) {
      word = textUtil.trimHex(word);
    } else if (word.length > 1) {  // Save index of separate characters
      return;
    }
  }

  if (textUtil.isWordInBlockList(word)) {
    console.log("Block word: '" + word + "'");
    result.blockWords++;
  }

  if (result.words.has(word)) {
    let val = result.words.get(word);
    val.r += textUtil.toRank(result.wordIdx) * factor;
    val.is.push(result.wordIdx++);
  } else {
      result.words.set(word, {r: textUtil.toRank(result.wordIdx) * factor, is:[result.wordIdx]});
      result.wordIdx++;
  }
}


function parseText(text, factor, result) {
  if (result.wordIdx > 30500)
    return;

  // Add text to result
  let cleanText = textUtil.cleanup(text);
  if (textUtil.canAddSpace(result.text))
    result.text += ' ';
  result.text += cleanText;

  // Add words to result
  const lst = textUtil.tokenize(cleanText);
  for(let word of lst) {
    if (word.length < 128)
      addWord(word, factor, result);
    else
      addWord(word.slice(0, 127), factor, result);

    if (result.wordIdx > 30500)
      return;
  }
}


function parseMeta(/*node, result*/) {
/*
  for (let attr of node.attrs) {
    // TODO
  }
*/
}


function parseTitle(node, result) {
  for (let child of node.childNodes) {
    if (!child.nodeName)
      continue;
    if (child.nodeName === '#text') {
      result.title = textUtil.cleanup(child.value);
      console.log("Title: " + result.title);
      parseText(child.value, titleRankFactor, result);
    }
  }
}


function parseHead(node, result) {
  //console.log(node);
  for (let child of node.childNodes) {
    if (!child.tagName)
      continue;
    if (child.tagName === 'meta') {
      parseMeta(child, result);
    } else if (child.tagName === 'title') {
      parseTitle(child, result);
    }
  }
}


function joinUrl(url, path) {
  try {
    let joinedUrl = new URL(path, url);
    return joinedUrl.toString();
  } catch (e) {
    console.error("Failed to join url " + url + " with path " + path);
    return undefined;
  }
}


async function parseFrame(blockSet, node, result) {
  //console.log('Frame:');
  //console.log(node);
  for (let attr of node.attrs) {
    if (attr.name === 'src') {
      const fullUrl = joinUrl(result.url, attr.value);
      if (fullUrl === undefined)
        return;
      console.log('Frame URL: ' + fullUrl);

      if (!await isAllowed(fullUrl)) {
        result.errors.push(fullUrl +' disallowed by robots.txt');
        return;
      }

      const frameResponse = await got(fullUrl, gotParams);
      //  result.url = pageResponse.url;
    
      console.log('StatusCode: ' + frameResponse.statusCode);
      console.log('URL: ' + frameResponse.url);
      
      if (frameResponse.statusCode != 200) {
        result.errors.push('Frame ' + fullUrl + ' status code ' + frameResponse.statusCode);
        return;
      }
      
      const latinDocument = parse5.parse(frameResponse.body);

      // TODO: filter them?
      result.charset = getCharset(latinDocument);
      console.log('Charset: ' + result.charset);
    
      const utf8Body = textUtil.toUtf8(frameResponse.body, result.charset);
      if (!await isRightDocumentLanguage(utf8Body, result)) {
        console.warn("Wrong document language");
        return;
      }
    
      const utf8Document = parse5.parse(utf8Body);
    
      const originalUrl = result.url;
      result.url = frameResponse.url;
      parseDocument(blockSet, utf8Document, result);
      result.url = originalUrl;

      break;
    }
  }
}


async function parseFrameSet(blockSet, node, result) {
  //console.log(node);
  for (let child of node.childNodes) {
    if (!child.tagName)
      continue;
    if (child.tagName === 'frame')
      await parseFrame(blockSet, child, result);
    else if (child.tagName === 'frameset')
      await parseFrameSet(blockSet, child, result);
  }
}


async function parseLink(blockSet, link, result) {
  let href;
  let follow = true;
  for (let attr of link.attrs) {
    if (attr.name === 'lang' || attr.name === 'hreflang') {
      if (textUtil.isWrongLanguage(attr.value)) {
        //console.log('Wrong language: ' + attr.value);
        //result.errors.push('Wrong language: ' + attr.value);
        follow = false;
        break;
      }
    } else if (attr.name === 'rel' && attr.value ==='nofollow') {
      follow = false;
      break;
    } else if (attr.name === 'href') {
      href = attr.value;
    }
  }

  if (!(follow && href))
    return;
  if (href === 'about:blank' || href === 'javascript:void(0)')
    return;
  if (href.startsWith('mailto:') || href.startsWith('jabber:'))
    return;

  if (textUtil.isDocument(href)) {
    const joinedUrl = joinUrl(result.url, href);
    if (!joinedUrl)
      return;
    if (!joinedUrl.startsWith('http'))
     return;
    const smartUrl = new URL(joinedUrl);
    if (blockSet.has(smartUrl.hostname))
      return;
    try {
      const decodedUrl = decodeURIComponent(decodeURI(joinedUrl));
      const clampedUrl = textUtil.clampURL(decodedUrl);

      result.urls.push(clampedUrl);
    } catch (e) {
      console.log(href);
      console.log(joinedUrl);
      console.error(e);
    }
  }
}


function isIterable(obj) {
  // checks for null and undefined
  if (obj == null) {
    return false;
  }
  return typeof obj[Symbol.iterator] === 'function';
}


function parseContainer(blockSet, node, result) {
  if (node.tagName === 'script' || node.tagName === 'style' || node.tagName === 'noscript')
    return;

  if (isIterable(node.attrs)) {
    for (let attr of node.attrs) {
      // Don't parse tooltips
      /*if (attr.name === 'title')
        parseText(attr.value, getNodeRankFactor(node.tagName), result);*/
      if (attr.name === 'alt' && node.tagName === 'img')
        parseText(attr.value, getNodeRankFactor('img'), result);
    }
  }

  if (node.tagName === 'a')
    parseLink(blockSet, node, result);

  if (!isIterable(node.childNodes))
    return;

  for (let child of node.childNodes) {
    if (child.nodeName === '#text')
      parseText(child.value, getNodeRankFactor(node.tagName), result);

    parseContainer(blockSet, child, result);
  }
}


async function parseHTML(blockSet, node, result) {
  //console.log(node);
  for (let attr of node.attrs) {
    if (attr.name === 'lang') {
      if (textUtil.isWrongLanguage(attr.value)) {
        console.log('Wrong language: ' + attr.value);
        result.errors.push('Wrong language: ' + attr.value);
        return;
      }
    }
  }

  for (let child of node.childNodes) {
    if (!child.tagName)
      continue;
    if (child.tagName === 'head')
      parseHead(child, result);
    else if (child.tagName === 'frameset')
      await parseFrameSet(blockSet, child, result);    
    else if (child.tagName === 'body')
      parseContainer(blockSet, child, result);    
  }
}


async function parseDocument(blockSet, node, result) {
  for (let child of node.childNodes) {
    if (!child.tagName)
      continue;
    if (child.tagName === 'html')
      await parseHTML(blockSet, child, result);    
  }
}


async function checkUrls(result) {
  const urlSet = new Set(result.urls);
  const uncheckedUrls = [...urlSet];
  result.urls = [];

  for (let i = 0; i < uncheckedUrls.length; i++) {
    const outLink = uncheckedUrls[i];
    // Links to itself
    if (outLink === result.url)
      continue;

    if (await isAllowedFast(outLink))
      result.urls.push(outLink);
    else
      console.log('Disallowed: ' + outLink);
  }
}

async function isRightDocumentLanguage(body, result) {
  const beforeLanguageDetect = Date.now();
  const detectionResult = await cld.detect(body, {isHTML: true, encodingHint: 'UTF8'});
  const afterLanguageDetect = Date.now();

  let mainLanguage;
  let mainLanguageCode;
  let maxPercent = 0;
  for (let lang of detectionResult.languages) {
    if (lang.percent > maxPercent) {
      maxPercent = lang.percent;
      mainLanguage = lang.name;
      mainLanguageCode = lang.code;
    }
  }

  console.log("Language detection taken " + (afterLanguageDetect - beforeLanguageDetect) + "ms. Main language: " + mainLanguage);

  const rightLanguage = ['ru', 'en'].includes(mainLanguageCode);
  if (!rightLanguage)
    result.errors.push('Wrong language: ' + mainLanguage);

  return rightLanguage;
}

async function scrapeUnsafe(blockSet, url) {
  console.log("Url: " + url);

  const result = {url: url, title:'', text: '', urls: [], errors: [], words: new Map(), blockWords: 0, badWords: 0, wordIdx: 1};

  if (!await isAllowed(url)) {
    result.errors.push('Disallowed by robots.txt');
    return result;
  }

  const pageResponse = await got(url, gotParams);

  console.log('StatusCode: ' + pageResponse.statusCode);
  console.log('URL: ' + pageResponse.url);

  if (pageResponse.url)
    result.url = decodeURIComponent(decodeURI(pageResponse.url));
  else
    result.url = url;
  
  if (pageResponse.statusCode != 200) {
    result.errors.push(pageResponse.statusCode);
    return result;
  }

  const latinDocument = parse5.parse(pageResponse.body);

  // TODO: filter them?
  result.charset = getCharset(latinDocument);
  console.log('Charset: ' + result.charset);

  const utf8Body = textUtil.toUtf8(pageResponse.body, result.charset);
  if (!await isRightDocumentLanguage(utf8Body, result)) {
    console.warn("Wrong document language");
    return result;
  }

  textUtil.addWordsFromUrl(result.url, result.words);

  const utf8Document = parse5.parse(utf8Body);

  await parseDocument(blockSet, utf8Document, result);

  result.title = result.title.slice(0, 500);
  result.text = result.text.slice(0, 1000000);

  await checkUrls(result);

  return result;
}


exports.scrape = async function (blockSet, url) {
  try {
    console.log(robotsTxtCache.itemCount + " items are in robotsTxtCache...");
    return await scrapeUnsafe(blockSet, url);
  } catch (e) {
    console.error(e);
    return {'url':url, title:'', text:'', urls:[], words: new Map(), blockWords:0, badWords: 0, errors: []};
  }
}


async function test() {
  const res = await exports.scrape(new Set(), 'https://nicolaiarocci.com/index.xml');
  for (let w of res.words.keys()) {
    res.words.set(w, JSON.stringify(res.words.get(w)));
  }
  console.log(res);
  process.exit(0);
}


//test();
